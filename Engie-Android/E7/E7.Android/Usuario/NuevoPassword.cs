﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Threading;
using E7.Entidades;
using E7.Servicio;
using E7.Util;
using ML;

namespace E7.Droid.Usuario
{
    [Activity(Label = "NpassActivity", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class NuevoPassword : Activity
    {
        ProgressDialog progress;
        Button Continuar;
        EditText NPass, CPass;
        string _usuario, _codigo;
        bool validar;
        BEUsuarioLogin resultado;
        UTComunes util = new UTComunes();
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Layout.NuevoPassword);

            Continuar = FindViewById<Button>(Resource.Id.button1);
            NPass = FindViewById<EditText>(Resource.Id.editText1);
            CPass = FindViewById<EditText>(Resource.Id.editText2);

            _usuario = Intent.GetStringExtra("_usuario") ?? "Data not available";
            _codigo = Intent.GetStringExtra("_codigo") ?? "Data not available";

            Continuar.Click += (sender, e) => login();

            void Cargando(string mensaje)
            {
                progress = ProgressDialog.Show(this, "", mensaje, true, false);
                progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            }
            void login()
            {
                if (!string.IsNullOrEmpty(NPass.Text) && !string.IsNullOrEmpty(CPass.Text) && !string.IsNullOrEmpty(_usuario))
                {
                    if (NPass.Text == CPass.Text)
                    {
                        Cargando("Restaurando contraseña...");
                        new Thread(new ThreadStart(delegate
                        {
                            RunOnUiThread(async () => await CambiarClave());
                        })).Start();
                    }
                    else
                    {
                        Toast.MakeText(this, "La contraseña no coincide", ToastLength.Long).Show();
                    }

                }
                else if (!string.IsNullOrEmpty(_usuario))
                {
                    Finish();
                    Toast.MakeText(this, "No se encontro el usuario en memoria", ToastLength.Long).Show();
                }
                else if (!string.IsNullOrEmpty(NPass.Text))
                {
                    NPass.RequestFocus();
                    Toast.MakeText(this, "Ingrese la nueva contraseña", ToastLength.Long).Show();
                }
                else
                {
                    CPass.RequestFocus();
                    Toast.MakeText(this, "Confirmar nueva contraseña", ToastLength.Long).Show();
                }
            }

            async Task CambiarClave()
            {
                DALogin conexion = new DALogin();

                var IsConnected = await util.IsConnected();

                if (IsConnected == true)
                {
                    resultado = await conexion.CambiarClave(_usuario, CPass.Text);
                    if (resultado.ValidarOperacion == "true")
                    {
                        Finish();
                        var intent_data = new Intent(this, typeof(Login));
                        StartActivity(intent_data);
                    }
                    else
                    {
                        Toast.MakeText(this, resultado.MensajeError, ToastLength.Long).Show();
                    }
                }
                else if (IsConnected == false)
                {
                    Toast.MakeText(this, "En estos momentos no tiene servicio de internet, por favor verificar.", ToastLength.Long).Show();
                    progress.Dismiss();
                }
            }
        }
    }
}