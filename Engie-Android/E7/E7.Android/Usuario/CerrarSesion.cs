﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ML;

namespace E7.Droid.Usuario
{
    [Activity(Label = "", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]

    public class CerrarSesion : Fragment
    {
        Android.App.ProgressDialog progress;
        string _nombreCompleto, _filtro, _mes;
        public CerrarSesion(string nombre, string filtro)
        {
            _nombreCompleto = nombre;
            _filtro = filtro;
        }

        public event EventHandler CallCsesion;
        public void OnCallCsesion()
        {
            EventHandler handler = CallCsesion;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.Login, container, false);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            

        }
    }
}