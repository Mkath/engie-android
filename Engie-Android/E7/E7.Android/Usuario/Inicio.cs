﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using System.Threading;
using ML;

namespace E7.Droid.Usuario
{
    [Activity(Label = "InicioActivity", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class Inicio : Fragment
    {
        //DrawerLayout drawerLayout;
        //NavigationView navigationView;
        Button precons, prefac;
        TextView saludos;
        int pgdurum;
        Android.App.ProgressDialog progress;
        string _nombreCompleto, _filtro;

        public event EventHandler CallConsumo;

        public void OnCallConsumo()
        {
            EventHandler handler = CallConsumo;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public event EventHandler CallFacturacion;

        public void OnCallFacturacion()
        {
            EventHandler handler = CallFacturacion;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public Inicio(string nombre, string filtro)
        {
            _nombreCompleto = nombre;
            _filtro = filtro;
        }

        public override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetHasOptionsMenu(true);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.Inicio, container, false);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);

            precons = View.FindViewById<Button>(Resource.Id.button2);
            prefac = View.FindViewById<Button>(Resource.Id.button1);
            saludos = View.FindViewById<TextView>(Resource.Id.textView1);

            saludos.Text = _nombreCompleto + "";

            precons.Click += (sender, e) => PreConsumo();

            prefac.Click += (sender, e) => PreFacturacion();


            void PreConsumo()
            {
                OnCallConsumo();
            }
            void PreFacturacion()
            {
                OnCallFacturacion();
            }
        }
    }
}
