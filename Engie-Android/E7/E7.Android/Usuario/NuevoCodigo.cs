﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Threading;
using E7.Servicio;
using E7.Entidades;
using E7.Droid.Utiles;
using E7.Util;
using ML;

namespace E7.Droid.Usuario
{
    [Activity(Label = "ncodeActivity", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class NuevoCodigo : Activity
    {
        Button Continuar, NuevCodigo;
        EditText Codigo;
        TextView Mensaje;
        string _usuario, _codigo, _email;
        int _intentos = 0;
        ProgressDialog progress;
        DALogin conexion = new DALogin();
        BEUsuarioLogin resultado;
        UTComunes util = new UTComunes();
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Layout.NuevoCodigo);

            Continuar = FindViewById<Button>(Resource.Id.button1);
            NuevCodigo = FindViewById<Button>(Resource.Id.button2);
            Codigo = FindViewById<EditText>(Resource.Id.editText1);
            Mensaje = FindViewById<TextView>(Resource.Id.textView1);

            _usuario = Intent.GetStringExtra("_usuario") ?? "Data not available";
            _codigo = Intent.GetStringExtra("_codigo") ?? "Data not available";
            //_usuario = "vitpower2";
            //_codigo = "ABCDF";
            _email = Intent.GetStringExtra("_email") ?? "Data not available";
            Continuar.Click += (sender, e) => npass();
            NuevCodigo.Click += (sender, e) => ncode();
            Carga();

            void Cargando(string mensaje)
            {
                progress = ProgressDialog.Show(this, "", mensaje, true, false);
                progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            }
            void Carga()
            {
                Mensaje.Text = "Se envió un código a tu dirección de correo electrónico. Escríbelo aquí.";
                _intentos = 3;
            }

            void npass()
            {
                if (!string.IsNullOrEmpty(Codigo.Text))
                {
                    if (_intentos > 0)
                    {
                        ValidarCodigo();
                    }
                    else
                    {
                        Toast.MakeText(this, "Se alcanzó el límite de intentos", ToastLength.Long).Show();
                        Finish();
                    }
                }
                else
                {
                    Codigo.RequestFocus();
                    Toast.MakeText(this, "ingrese el código", ToastLength.Long).Show();
                }
            }
            void ValidarCodigo()
            {
                if (_codigo == Codigo.Text)
                {
                    Finish();
                    Toast.MakeText(this, "El código es correcto", ToastLength.Long).Show();
                    var intent_data = new Intent(this, typeof(NuevoPassword));
                    intent_data.PutExtra("_usuario", _usuario);
                    intent_data.PutExtra("_codigo", _codigo);
                    StartActivity(intent_data);
                }
                else
                {
                    _intentos = _intentos - 1;
                    Toast.MakeText(this, "El código no es válido", ToastLength.Long).Show();
                }
            }
            void ncode()
            {
                if (!string.IsNullOrEmpty(_usuario))
                {
                    Cargando("Generando código...");
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await GenerarToken());
                    })).Start();
                }
                else
                {
                    Toast.MakeText(this, "No se encontro usuario en memoria", ToastLength.Long).Show();
                }
            }
            async Task GenerarToken()
            {
                RandomPassword vl_RandomPassword = new RandomPassword();
                _codigo = await vl_RandomPassword.Generate();
                if (_codigo != null)
                {
                    progress.Dismiss();
                    Cargando("Enviado código...");
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await EnviarToken());
                    })).Start();
                }
                else
                {
                    Toast.MakeText(this, "Inténtelo nuevamente", ToastLength.Long).Show();
                }
            }
            async Task EnviarToken()
            {
                var IsConnected = await util.IsConnected();
                if (IsConnected == true)
                {
                    resultado = await conexion.EnviarMailToken(_usuario, _codigo);
                    if (resultado.ValidarOperacion == "true")
                    {
                        progress.Dismiss();
                        Toast.MakeText(this, resultado.MensajeError, ToastLength.Long).Show();
                        var intent_data = new Intent(this, typeof(NuevoCodigo));
                        intent_data.PutExtra("_usuario", resultado.Usuario);
                        intent_data.PutExtra("_email", resultado.Email);
                        intent_data.PutExtra("_codigo", _codigo);
                        StartActivity(intent_data);
                    }
                    else
                    {
                        progress.Dismiss();
                        Toast.MakeText(this, resultado.MensajeError, ToastLength.Long).Show();
                    }
                }
                else if (IsConnected == false)
                {
                    Toast.MakeText(this, "En estos momentos no tiene servicio de internet, por favor verificar.", ToastLength.Long).Show();
                    progress.Dismiss();
                }

            }
        }
    }
}