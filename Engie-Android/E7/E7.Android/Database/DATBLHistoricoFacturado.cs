﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using E7.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E7.Droid.Database
{
    public class DATBLHistoricoFacturado
    {
        string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

        public Task<bool> CreateTableHF()
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        connection.CreateTable<TBLHistoricoFacturado>();
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return false;
                }
            });
        }

        public Task<bool> InsertTableHF(TBLHistoricoFacturado hf)
        {
            return Task.Run(() =>
            {
                bool salida = true;
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        var list = connection.Query<TBLHistoricoFacturado>("SELECT * FROM TBLHistoricoFacturado WHERE Fecha=? AND CodCliente=? AND CodPuntoFacturacion=? AND Moneda=? AND Concepto=?", hf.Fecha, hf.CodCliente, hf.CodPuntoFacturacion, hf.Moneda, hf.Concepto);
                        if (list.Count == 0)
                        {
                            connection.Insert(hf);
                        }
                        return salida;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return salida = false; ;
                }
            });
        }

        public Task<List<TBLHistoricoFacturado>> SelectTableHF(TBLHistoricoFacturado hf, DateTime desde, DateTime hasta)
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLHistoricoFacturado> list = new List<TBLHistoricoFacturado>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Query<TBLHistoricoFacturado>("SELECT * FROM TBLHistoricoFacturado WHERE CodCliente=? AND CodPuntoFacturacion=? AND Moneda=? AND Fecha>? AND Fecha<?", hf.CodCliente, hf.CodPuntoFacturacion, hf.Moneda, desde.Ticks, hasta.Ticks);
                        return list;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return null;
                }
            });
        }

        public Task<List<TBLHistoricoFacturado>> SelectTableHF(TBLHistoricoFacturado hf)
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLHistoricoFacturado> list = new List<TBLHistoricoFacturado>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Query<TBLHistoricoFacturado>("SELECT * FROM TBLHistoricoFacturado WHERE CodCliente=? AND CodPuntoFacturacion=? AND Moneda=?", hf.CodCliente, hf.CodPuntoFacturacion, hf.Moneda);
                        return list;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return null;
                }
            });
        }

        public Task<List<TBLHistoricoFacturado>> ValidateTableHF()
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLHistoricoFacturado> list = new List<TBLHistoricoFacturado>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Table<TBLHistoricoFacturado>().ToList(); return list;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return null;
                }
            });
        }

        public Task<bool> DeleteTableHF(DateTime date)
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        connection.Query<TBLHistoricoFacturado>("DELETE FROM TBLHistoricoFacturado WHERE Fecha<?", date.Ticks);
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return false;
                }
            });
        }
    }
}