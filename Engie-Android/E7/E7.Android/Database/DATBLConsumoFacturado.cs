﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using Android.Util;
using System.Threading.Tasks;
using E7.Model;

namespace E7.Droid.Database
{
    public class DATBLConsumoFacturado
    {
        string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

        public Task<bool> CreateTableCF()
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        connection.CreateTable<TBLConsumoFacturado>();
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return false;
                }
            });
        }

        public Task<bool> InsertTableDF(TBLConsumoFacturado cf)
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        var list = connection.Query<TBLDistribucionFacturada>("SELECT * FROM TBLConsumoFacturado WHERE CodCliente=? AND CodPuntoFacturacion=? AND Fecha=?", cf.CodCliente, cf.CodPuntoFacturacion, cf.Fecha);

                        if (list.Count == 0)
                        {
                            connection.Insert(cf);
                        }
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return false;
                }
            });
        }

        public Task<List<TBLConsumoFacturado>> SelectTableCF(TBLConsumoFacturado cf)
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLConsumoFacturado> list = new List<TBLConsumoFacturado>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Query<TBLConsumoFacturado>("SELECT * FROM TBLConsumoFacturado WHERE Anio=? AND CodCliente=? AND CodPuntoFacturacion=?", cf.Anio, cf.CodCliente, cf.CodPuntoFacturacion);
                        return list;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return null;
                }
            });
        }
        public Task<List<TBLConsumoFacturado>> SelectTableTodoCF(TBLConsumoFacturado cf)
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLConsumoFacturado> list = new List<TBLConsumoFacturado>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Query<TBLConsumoFacturado>("SELECT * FROM TBLConsumoFacturado WHERE CodCliente=? AND CodPuntoFacturacion=?", cf.CodCliente, cf.CodPuntoFacturacion);
                        return list;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return null;
                }
            });
        }

        public Task<List<TBLConsumoFacturado>> ValidateTableCF()
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLConsumoFacturado> list = new List<TBLConsumoFacturado>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Table<TBLConsumoFacturado>().ToList();
                        return list;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return null;
                }
            });
        }

        public Task<bool> DeleteTableDF(DateTime date)
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        connection.Query<TBLConsumoFacturado>("DELETE FROM TBLConsumoFacturado WHERE Fecha<?", date.Ticks);
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return false;
                }
            });
        }

    }
}