﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using E7.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E7.Droid.Database
{
    public class DATBLDistribucionFacturada
    {
        string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

        public Task<bool> CreateTableDF()
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        connection.CreateTable<TBLDistribucionFacturada>();
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return false;
                }
            });
        }

        public Task<bool> InsertTableDF(TBLDistribucionFacturada df)
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        var list = connection.Query<TBLDistribucionFacturada>("SELECT * FROM TBLDistribucionFacturada WHERE Mes=? AND Anio=? AND CodCliente=? AND CodPuntoFacturacion=? AND Concepto=?", df.Mes, df.Anio, df.CodCliente, df.CodPuntoFacturacion, df.Concepto);
                        if (list.Count == 0)
                        {
                            connection.Insert(df);
                        }
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return false;
                }
            });
        }

        public Task<List<TBLDistribucionFacturada>> SelectTableDF(TBLDistribucionFacturada df)
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLDistribucionFacturada> list = new List<TBLDistribucionFacturada>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Query<TBLDistribucionFacturada>("SELECT * FROM TBLDistribucionFacturada WHERE Mes=? AND Anio=? AND CodCliente=? AND CodPuntoFacturacion=?", df.Mes, df.Anio, df.CodCliente, df.CodPuntoFacturacion);
                        return list;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return null;
                }
            });
        }

        public Task<List<TBLDistribucionFacturada>> SelectTableTodoDF(TBLDistribucionFacturada df)
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLDistribucionFacturada> list = new List<TBLDistribucionFacturada>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Query<TBLDistribucionFacturada>("SELECT * FROM TBLDistribucionFacturada WHERE CodCliente=? AND CodPuntoFacturacion=?", df.CodCliente, df.CodPuntoFacturacion);
                        return list;

                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return null;
                }
            });
        }

        public Task<List<TBLDistribucionFacturada>> ValidateTableDF()
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLDistribucionFacturada> list = new List<TBLDistribucionFacturada>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Table<TBLDistribucionFacturada>().ToList();
                        return list;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return null;
                }
            });
        }

        public Task<bool> DeleteTableDF(DateTime date)
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        connection.Query<TBLDistribucionFacturada>("DELETE FROM TBLDistribucionFacturada WHERE Fecha<?", date.Ticks);
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return false;
                }
            });
        }
    }
}