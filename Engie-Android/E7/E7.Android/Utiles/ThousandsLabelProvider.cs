﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using SciChart.Core.Utility;
using SciChart.Charting.Visuals.Axes;
using SciChart.Charting.Numerics.LabelProviders;

namespace E7.Droid.Utiles
{
    public class ThousandsLabelProvider : NumericLabelProvider
    {
        public override ICharSequence FormatLabelFormatted(Java.Lang.IComparable dataValue)
        {
            return new Java.Lang.String(base.FormatLabelFormatted((Java.Lang.Double)(ComparableUtil.ToDouble(dataValue) / 1000d)) + "K");
        }
    }

    public class MillionsLabelProvider : NumericLabelProvider
    {
        public override ICharSequence FormatLabelFormatted(Java.Lang.IComparable dataValue)
        {
            return new Java.Lang.String(base.FormatLabelFormatted((Java.Lang.Double)(ComparableUtil.ToDouble(dataValue) / 1000000d)) + "M");
        }
    }

    public class MonthLabelProvider : FormatterLabelProviderBase
    {
        public MonthLabelProvider() : base(typeof(NumericAxis).ToClass(), new MonthLabelFormatter())
        {
        }
    }
    public class MonthLabelFormatter : Java.Lang.Object, ILabelFormatter
    {
        public void Update(Java.Lang.Object axis)
        {
        }

        public ICharSequence FormatLabelFormatted(Java.Lang.IComparable dataValue)
        {
            string fecha = (ComparableUtil.ToDouble(dataValue)).ToString();
            string MesLabel = "";
            if (fecha.Length > 0 && fecha.Length < 3)
            {
                int mes = Convert.ToInt32(fecha);
                switch (mes)
                {
                    case 1:
                        MesLabel = "Ene";
                        break;
                    case 2:
                        MesLabel = "Feb";
                        break;
                    case 3:
                        MesLabel = "Mar";
                        break;
                    case 4:
                        MesLabel = "Abr";
                        break;
                    case 5:
                        MesLabel = "May";
                        break;
                    case 6:
                        MesLabel = "Jun";
                        break;
                    case 7:
                        MesLabel = "Jul";
                        break;
                    case 8:
                        MesLabel = "Ago";
                        break;
                    case 9:
                        MesLabel = "Sep";
                        break;
                    case 10:
                        MesLabel = "Oct";
                        break;
                    case 11:
                        MesLabel = "Nov";
                        break;
                    case 12:
                        MesLabel = "Dic";
                        break;
                }
            }
            return new Java.Lang.String(MesLabel);
        }

        public ICharSequence FormatCursorLabelFormatted(Java.Lang.IComparable dataValue)
        {
            var fecha = (ComparableUtil.ToDouble(dataValue));

            int mes = Convert.ToInt32(fecha);
            string MesLabel = "";
            switch (mes)
            {
                case 1:
                    MesLabel = "Enero";
                    break;
                case 2:
                    MesLabel = "Febrero";
                    break;
                case 3:
                    MesLabel = "Marzo";
                    break;
                case 4:
                    MesLabel = "Abril";
                    break;
                case 5:
                    MesLabel = "Mayo";
                    break;
                case 6:
                    MesLabel = "Junio";
                    break;
                case 7:
                    MesLabel = "Julio";
                    break;
                case 8:
                    MesLabel = "Agosto";
                    break;
                case 9:
                    MesLabel = "Septiembre";
                    break;
                case 10:
                    MesLabel = "Octubre";
                    break;
                case 11:
                    MesLabel = "Noviembre";
                    break;
                case 12:
                    MesLabel = "Diciembre";
                    break;
            }
            return new Java.Lang.String(MesLabel);
        }
    }

    public class MonthYearLabelProvider : FormatterLabelProviderBase
    {
        public MonthYearLabelProvider() : base(typeof(NumericAxis).ToClass(), new MonthYearLabelFormatter())
        {
        }
    }
    public class MonthYearLabelFormatter : Java.Lang.Object, ILabelFormatter
    {
        public void Update(Java.Lang.Object axis)
        {
        }

        public ICharSequence FormatLabelFormatted(Java.Lang.IComparable dataValue)
        {
            string _double = (ComparableUtil.ToDouble(dataValue)).ToString();
            string MesLabel = "";
            if (_double.Length == 8)
            {
                int anio = Convert.ToInt32(_double.Substring(0, 4));
                int mes = Convert.ToInt32(_double.Substring(4, 2));
                int count = Convert.ToInt32(_double.Substring(6, 2));
                DateTime fecha = new DateTime(anio, mes, 1);
                DateTime fechaFinal = fecha.AddMonths(count);
                switch (fechaFinal.Month)
                {
                    case 1:
                        MesLabel = "Ene. " + fechaFinal.Year;
                        break;
                    case 2:
                        MesLabel = "Feb. " + fechaFinal.Year;
                        break;
                    case 3:
                        MesLabel = "Mar. " + fechaFinal.Year;
                        break;
                    case 4:
                        MesLabel = "Abr. " + fechaFinal.Year;
                        break;
                    case 5:
                        MesLabel = "May. " + fechaFinal.Year;
                        break;
                    case 6:
                        MesLabel = "Jun. " + fechaFinal.Year;
                        break;
                    case 7:
                        MesLabel = "Jul. " + fechaFinal.Year;
                        break;
                    case 8:
                        MesLabel = "Ago. " + fechaFinal.Year;
                        break;
                    case 9:
                        MesLabel = "Sep. " + fechaFinal.Year;
                        break;
                    case 10:
                        MesLabel = "Oct. " + fechaFinal.Year;
                        break;
                    case 11:
                        MesLabel = "Nov. " + fechaFinal.Year;
                        break;
                    case 12:
                        MesLabel = "Dic. " + fechaFinal.Year;
                        break;
                }
            }
            return new Java.Lang.String(MesLabel);
        }

        public ICharSequence FormatCursorLabelFormatted(Java.Lang.IComparable dataValue)
        {
            string _double = (ComparableUtil.ToDouble(dataValue)).ToString();
            string MesLabel = "";
            if (_double.Length == 8)
            {
                int anio = Convert.ToInt32(_double.Substring(0, 4));
                int mes = Convert.ToInt32(_double.Substring(4, 2));
                int count = Convert.ToInt32(_double.Substring(6, 2));
                DateTime fecha = new DateTime(anio, mes, 1);
                DateTime fechaFinal = fecha.AddMonths(count);
                switch (fechaFinal.Month)
                {
                    case 1:
                        MesLabel = "Ene. " + fechaFinal.Year;
                        break;
                    case 2:
                        MesLabel = "Feb. " + fechaFinal.Year;
                        break;
                    case 3:
                        MesLabel = "Mar. " + fechaFinal.Year;
                        break;
                    case 4:
                        MesLabel = "Abr. " + fechaFinal.Year;
                        break;
                    case 5:
                        MesLabel = "May. " + fechaFinal.Year;
                        break;
                    case 6:
                        MesLabel = "Jun. " + fechaFinal.Year;
                        break;
                    case 7:
                        MesLabel = "Jul. " + fechaFinal.Year;
                        break;
                    case 8:
                        MesLabel = "Ago. " + fechaFinal.Year;
                        break;
                    case 9:
                        MesLabel = "Sep. " + fechaFinal.Year;
                        break;
                    case 10:
                        MesLabel = "Oct. " + fechaFinal.Year;
                        break;
                    case 11:
                        MesLabel = "Nov. " + fechaFinal.Year;
                        break;
                    case 12:
                        MesLabel = "Dic. " + fechaFinal.Year;
                        break;
                }
            }
            return new Java.Lang.String(MesLabel);
        }
    }
}