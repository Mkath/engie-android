﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SciChart.Charting.Visuals.RenderableSeries;

namespace E7.Droid.Utiles
{
    class ChartTypeModel
    {
        public ChartTypeModel(StackedSeriesCollectionBase seriesCollection, string header,string title,string medida)
        {
            SeriesCollection = seriesCollection;
            TypeName = new Java.Lang.String(header);
            Title = new Java.Lang.String(title);
            Medida = new Java.Lang.String(medida);
        }

        public StackedSeriesCollectionBase SeriesCollection { get; }
        public Java.Lang.String TypeName { get; }
        public Java.Lang.String Title { get; }
        public Java.Lang.String Medida { get; }
    }
}