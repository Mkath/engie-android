﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using E7.Entidades;
using ML;

namespace E7.Droid.Utiles
{
    public class MedidorAdapter : BaseAdapter
    {
        private Context c;
        private JavaList<BEMedidor> medidores;
        private LayoutInflater inflater;

        public MedidorAdapter(Context c, JavaList<BEMedidor> medidores)
        {
            this.c = c;
            this.medidores = medidores;
        }
        public override Java.Lang.Object GetItem(int position)
        {
            return medidores.Get(position);
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            if (inflater == null)
            {
                inflater = (LayoutInflater) c.GetSystemService(Context.LayoutInflaterService);
            }
            if (convertView == null)
            {
                convertView = inflater.Inflate(Resource.Layout.ResourceSpinner,parent,false);
            }

            TextView cod = convertView.FindViewById<TextView>(Resource.Id.cod1);
            TextView nomb = convertView.FindViewById<TextView>(Resource.Id.nom1);
            cod.Text = medidores[position].Codigo;
            nomb.Text = medidores[position].Descripcion;
            return convertView;
        }
        public override int Count
        {
            get { return medidores.Size(); }
        }
    }

}