﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using SciChart.Charting.Visuals;
using SciChart.Charting.Visuals.Axes;
using SciChart.Charting.Model.DataSeries;
using SciChart.Charting.Visuals.RenderableSeries;
using SciChart.Charting.Modifiers;
using SciChart.Drawing.Common;
using System.Threading.Tasks;
using System.Threading;
using SciChart.Data.Model;
using SciChart.Charting.Visuals.Legend;
using SciChart.Charting.Themes;
using E7.Droid.Database;
using System.Globalization;
using E7.Servicio;
using E7.Entidades;
using E7.Model;
using ML;
using E7.Droid.Utiles;
using System.Diagnostics;

namespace E7.Droid.Facturacion
{
    [Activity(Label = "", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class HistPrecio_G : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        string _usuarioInterno, filtro;
        Android.App.ProgressDialog progress;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.HistPrecio_G);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(GetDrawable(Resource.Drawable.backButton));

            TextView ToolOff = FindViewById<TextView>(Resource.Id.modooff);
            //var chart = FindViewById<SciChartSurface>(Resource.Id.Chart);
            var chart = new SciChartSurface(this);
            var linearLayoutC = FindViewById<LinearLayout>(Resource.Id.linearLayout1);
            var linearLayoutL = FindViewById<LinearLayout>(Resource.Id.linearLayout2);
            int themeid = Resource.Style.SciChart_Bright_Spark;
            chart.Theme = themeid;


            _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            filtro = Intent.GetStringExtra("filtro") ?? "Data not available";
            string _codCliente = Intent.GetStringExtra("_codCliente") ?? "Data not available";
            string _codPuntoF = Intent.GetStringExtra("_codPuntoF") ?? "Data not available";
            string _moneda = Intent.GetStringExtra("_moneda") ?? "Data not available";
            string _fechaD = Intent.GetStringExtra("_fechaD") ?? "Data not available";
            string _fechaH = Intent.GetStringExtra("_fechaH") ?? "Data not available";
            string _modooff = Intent.GetStringExtra("_modooff") ?? "0";

            ModoOff();
            Inicializar();
            void ModoOff()
            {
                if (_modooff == "0")
                {
                    ToolOff.Visibility = ViewStates.Gone;
                }
                else
                {
                    ToolOff.Visibility = ViewStates.Visible;
                }
            }
            void Inicializar()
            {
                progress = ProgressDialog.Show(this, "", "Obteniendo datos...", true, false);
                progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                new Thread(new ThreadStart(delegate
                {
                    //RunOnUiThread(async () => await CargarGrafico());
                    RunOnUiThread(async () => await CargarGrafico());

                })).Start();
            }

            async Task CargarGrafico()
            {
                List<BEGraficosFacturacion> resultado;
                List<TBLHistoricoPrecio> resultado1;
                DAReporte_Facturacion conexion = new DAReporte_Facturacion();
                BLTBLHistoricoPrecio conexion1 = new BLTBLHistoricoPrecio();
                TBLHistoricoPrecio data = new TBLHistoricoPrecio();
                string mhp;
                string MensajeErrorHP;

                if (_moneda == "USD") { mhp = "$ "; } else if (_moneda == "PEN") { mhp = "S/ "; } else { mhp = ""; }
                if (_modooff == "0")
                {
                    resultado = await conexion.Reporte_HistoricoPrecio(_fechaD, _fechaH, _codCliente, _codPuntoF, _moneda);
                    MensajeErrorHP = conexion.ResultadoFinalRHP;
                    if (resultado == null)
                    {
                        progress.Dismiss();
                        Toast.MakeText(this, MensajeErrorHP, ToastLength.Short).Show();
                    }
                    else if (resultado.Count == 0)
                    {
                        progress.Dismiss();
                        Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                        alert.SetMessage("No hay datos.");
                        alert.SetCancelable(false);
                        alert.SetPositiveButton("OK", (senderAlert, args) =>
                        {
                            Finish();
                        });
                        alert.Show();
                    }
                    else
                    {
                        var Fecha = resultado.OrderBy(x => x.Fecha).Select(x => x.Fecha).Distinct().ToArray();
                        var Potencia = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Potencia").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var EnergiaHoraPunta = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaHoraPunta").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var EnergiaHoraFueraPunta = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaHoraFueraPunta").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var PrecioMedio = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Monomico").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var PCSPT = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "PCSPT").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var SST = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "SST").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var cont = (Convert.ToDouble(resultado.Count) / Convert.ToDouble(6));
                        GraficarColumn(Fecha, Potencia, EnergiaHoraPunta, EnergiaHoraFueraPunta, PrecioMedio, PCSPT, SST, cont);
                    }
                }
                else if (_modooff != "0")
                {
                    DateTime desde = DateTime.ParseExact(_fechaD, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime _desde = new DateTime(desde.Year, desde.Month, 1);
                    DateTime _Fdesde = new DateTime(desde.Year, desde.Month, 20);
                    DateTime hasta = DateTime.ParseExact(_fechaH, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime _hasta = new DateTime(hasta.Year, hasta.Month, 20);
                    data.CodCliente = _codCliente;
                    data.CodPuntoFacturacion = _codPuntoF;
                    data.Moneda = _moneda;
                    resultado1 = await conexion1.Reporte_HistoricoPrecio(data, _desde, _hasta);
                    var _ultimoMes = resultado1.Where(x => (x.Fecha <= _Fdesde)).ToArray();
                    MensajeErrorHP = "Error al momento de leer los datos guardados";
                    if (resultado1 == null)
                    {
                        progress.Dismiss();
                        Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                        alert.SetTitle("Modo OffLine");
                        alert.SetMessage("No hay datos sincronizados.");
                        alert.SetCancelable(false);
                        alert.SetPositiveButton("OK", (senderAlert, args) =>
                        {
                            Finish();
                        });
                        alert.Show();
                    }
                    else if (resultado1.Count == 0 || _ultimoMes.Count() == 0)
                    {
                        resultado1 = await conexion1.Reporte_HistoricoPrecio(data);
                        if (resultado1 == null)
                        {
                            progress.Dismiss();
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                            alert.SetTitle("Modo OffLine");
                            alert.SetMessage("No hay datos sincronizados.");
                            alert.SetCancelable(false);
                            alert.SetPositiveButton("OK", (senderAlert, args) =>
                            {
                                Finish();
                            });
                            alert.Show();
                        }
                        else if (resultado1.Count == 0)
                        {
                            progress.Dismiss();
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                            alert.SetTitle("Modo OffLine");
                            alert.SetMessage("No hay datos sincronizados.");
                            alert.SetCancelable(false);
                            alert.SetPositiveButton("OK", (senderAlert, args) =>
                            {
                                Finish();
                            });
                            alert.Show();
                        }
                        else
                        {
                            string _meses = "";
                            if (_modooff == "1") { _meses = " mes."; } else { _meses = " meses."; }
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                            alert.SetTitle("Modo OffLine");
                            alert.SetMessage("Usted solo tiene datos sincronizados de " + _modooff + _meses);
                            alert.SetCancelable(false);
                            alert.SetPositiveButton("OK", (senderAlert, args) =>
                            {
                                var Fecha = resultado1.OrderBy(x => x.Fecha).Select(x => x.Fecha).Distinct().ToArray();
                                var Potencia = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Potencia").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                                var EnergiaHoraPunta = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaHoraPunta").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                                var EnergiaHoraFueraPunta = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaHoraFueraPunta").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                                var PrecioMedio = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Monomico").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                                var PCSPT = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "PCSPT").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                                var SST = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "SST").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                                var cont = (Convert.ToDouble(resultado1.Count) / Convert.ToDouble(6));
                                GraficarColumn(Fecha, Potencia, EnergiaHoraPunta, EnergiaHoraFueraPunta, PrecioMedio, PCSPT, SST, cont);

                            });
                            alert.Show();
                        }
                    }
                    else
                    {
                        var Fecha = resultado1.OrderBy(x => x.Fecha).Select(x => x.Fecha).Distinct().ToArray();
                        var Potencia = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Potencia").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var EnergiaHoraPunta = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaHoraPunta").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var EnergiaHoraFueraPunta = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaHoraFueraPunta").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var PrecioMedio = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Monomico").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var PCSPT = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "PCSPT").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var SST = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "SST").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var cont = (Convert.ToDouble(resultado1.Count) / Convert.ToDouble(6));
                        GraficarColumn(Fecha, Potencia, EnergiaHoraPunta, EnergiaHoraFueraPunta, PrecioMedio, PCSPT, SST, cont);
                    }
                }
                void GraficarColumn(DateTime[] Fecha, double[] Potencia, double[] EnergiaHoraPunta, double[] EnergiaHoraFueraPunta, double[] PrecioMedio, double[] PCSPT, double[] SST, double cont)
                {
                    try
                    {
                        var xAxis = new NumericAxis(this)
                        {
                            GrowBy = new DoubleRange(-0, -0),
                            //VisibleRangeLimit = new DoubleRange(0,0),
                            //AutoRange = AutoRange.Always,
                            DrawMajorGridLines = false,
                            DrawMinorGridLines = false,
                            LabelProvider = new MonthYearLabelProvider(),
                            AxisBandsStyle = new SolidBrushStyle(0xfff9f9f9),
                        };
                        var yAxis = new NumericAxis(this)
                        {
                            AxisTitle = "Consumo" + " (" + _moneda + ")",
                            GrowBy = new DoubleRange(0, 0.1d),
                            AutoRange = AutoRange.Always,
                            CursorTextFormatting = mhp + "##,##0.00",
                            DrawMajorGridLines = false,
                            DrawMinorGridLines = false,
                            AxisBandsStyle = new SolidBrushStyle(0xfff9f9f9),
                            AxisAlignment = AxisAlignment.Left,

                        };

                        var PotenciaDataSeries = new XyDataSeries<double, double> { SeriesName = "Potencia" };
                        var EnergiaHoraPuntaDataSeries = new XyDataSeries<double, double> { SeriesName = "Energia Hora Punta" };
                        var EnergiaHoraFueraPuntaDataSeries = new XyDataSeries<double, double> { SeriesName = "Energia Hora Fuera Punta" };
                        var PrecioMedioDataSeries = new XyDataSeries<double, double> { SeriesName = "Precio Medio" };
                        var PCSPTDataSeries = new XyDataSeries<double, double> { SeriesName = "PCSPT" };
                        var SSTDataSeries = new XyDataSeries<double, double> { SeriesName = "SST" };
                        var totalDataSeries = new XyDataSeries<double, double> { SeriesName = "Total" };

                        for (var i = 0; i < cont; i++)
                        {
                            string dia = "";
                            if (i.ToString().Length == 1)
                            {
                                dia = "0" + i;
                            }
                            else
                            {
                                dia = i.ToString();
                            }
                            string anio = Fecha[0].ToString("yyyy");
                            string mes = Fecha[0].ToString("MM");
                            double AnioMes = Convert.ToDouble(string.Format("{0}{1}{2}", anio, mes, dia));
							PotenciaDataSeries.Append(AnioMes, Potencia[i]);
							EnergiaHoraPuntaDataSeries.Append(AnioMes, EnergiaHoraPunta[i]);
							EnergiaHoraFueraPuntaDataSeries.Append(AnioMes, EnergiaHoraFueraPunta[i]);
							PrecioMedioDataSeries.Append(AnioMes, PrecioMedio[i]);
							PCSPTDataSeries.Append(AnioMes, PCSPT[i]);
							SSTDataSeries.Append(AnioMes, SST[i]);
							totalDataSeries.Append(AnioMes, Potencia[i] + EnergiaHoraPunta[i] + EnergiaHoraFueraPunta[i] + PrecioMedio[i] + PCSPT[i] + SST[i]);
                            //Debugger.Break();
                        }

                        //Debugger.Break();
                        var columnsCollection = new HorizontallyStackedColumnsCollection();

                        columnsCollection.Add(GetRenderableSeries(PotenciaDataSeries, 0xff7cb5ec, 0xff7cb5ec));
                        columnsCollection.Add(GetRenderableSeries(EnergiaHoraPuntaDataSeries, 0xFF434348, 0xFF434348));
                        columnsCollection.Add(GetRenderableSeries(EnergiaHoraFueraPuntaDataSeries, 0xFF90ed7d, 0xFF90ed7d));
                        columnsCollection.Add(GetRenderableSeries(PrecioMedioDataSeries, 0xFFf7a35c, 0xFFf7a35c));
                        columnsCollection.Add(GetRenderableSeries(PCSPTDataSeries, 0xFF8085e9, 0xFF8085e9));
                        columnsCollection.Add(GetRenderableSeries(SSTDataSeries, 0xfff15c80, 0xfff15c80));

                        var legend = new SciChartLegend(this);

                        var legendModifier = new LegendModifier(legend);
                        legendModifier.SetOrientation(SciChart.Core.Framework.Orientation.Vertical);
                        legendModifier.SetLegendPosition(GravityFlags.Top | GravityFlags.Start, 0);
                        legendModifier.SetSourceMode(SourceMode.AllVisibleSeries);
                        legendModifier.SetShowSeriesMarkers(true);
                        legendModifier.SetShowCheckboxes(true);
                        legendModifier.SetReceiveHandledEvents(true);

                        var modifierGroup = new ModifierGroup(legendModifier);

                        chart.ChartModifiers.Add(modifierGroup);
                        if (legend.Parent != null)
                        {
                            (legend.Parent as ViewGroup).RemoveView(legend);
                        }
                        chart.XAxes.Add(xAxis);
                        chart.YAxes.Add(yAxis);
                        chart.RenderableSeries.Add(columnsCollection);
                        //Curso
                        chart.ChartModifiers.Add(new TooltipModifier());
                        //Zoom
                        chart.ChartModifiers.Add(new PinchZoomModifier
                        {
                            Direction = SciChart.Charting.Direction2D.XDirection,
                            ScaleFactor = 0.4f,
                            IsUniformZoom = true,
                        });
                        //Volver al inicio
                        chart.ChartModifiers.Add(new ZoomExtentsModifier());
                        //Mover
                        chart.ChartModifiers.Add(new ZoomPanModifier
                        {
                            Direction = SciChart.Charting.Direction2D.XDirection,
                            ZoomExtentsY = false,
                            ClipModeX = SciChart.Charting.ClipMode.ClipAtExtents
                        });
                        linearLayoutC.Orientation = Orientation.Vertical;
                        linearLayoutL.Orientation = Orientation.Vertical;
                        linearLayoutC.AddView(chart);
                        linearLayoutL.AddView(legend);
                        progress.Dismiss();
                    }
                    catch (Exception ex)
                    {
                        progress.Dismiss();
                        Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
                    }
                }
            }
        }
        private StackedColumnRenderableSeries GetRenderableSeries(IDataSeries dataSeries, uint fillColor, uint strokeColor)
        {
            return new StackedColumnRenderableSeries
            {
                DataSeries = dataSeries,
                StrokeStyle = new SolidPenStyle(this, strokeColor),
                FillBrushStyle = new SolidBrushStyle(fillColor)
            };
        }

        //BackButton
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}