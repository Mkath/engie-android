﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using System.Threading;
using System.Threading.Tasks;
using E7.Droid.Database;
using E7.Servicio;
using E7.Entidades;
using E7.Droid.Utiles;
using ML;
using E7.Util;

namespace E7.Droid.Facturacion
{
    [Activity(Label = "", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class HistPrecio_F : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        ClientesAdapter adapter_c;
        PuntoFacturacionAdapter adapter_p;
        MesAdapter adapter_m;
        JavaList<BEClientes> clientes;
        JavaList<BEPuntoFacturacion> puntoFacturacion = new JavaList<BEPuntoFacturacion>();
        JavaList<Mes> mes;
        Spinner CodCliente, IdPuntoFacturacion, Moneda, mesD, anioD, mesH, anioH;
        Button Graficar;
        string _codCliente, _codPuntoF, _moneda, _mesD, _anioD, _fechaD, _mesH, _anioH, _fechaH, filtro, _usuarioInterno, _modooff;
        Thread thread = null;
        Android.App.ProgressDialog progress;
        DateTime date;
        int _array;
        string[] items;
        DAReporte_Facturacion conexion = new DAReporte_Facturacion();
        DATBLHistoricoPrecio DAhp = new DATBLHistoricoPrecio();
        UTComunes util = new UTComunes();
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            //Listar Clientes y Punto de Facturacion
            _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            filtro = Intent.GetStringExtra("filtro") ?? "Data not available";
            ISharedPreferences pref = Application.Context.GetSharedPreferences("UserInfo", FileCreationMode.Private);
            _modooff = pref.GetString("_modooff", "0");

            clientes = (JavaList<BEClientes>)Newtonsoft.Json.JsonConvert.DeserializeObject<JavaList<BEClientes>>(filtro);

            SetContentView(layoutResID: Resource.Layout.HistPrecio_F);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(GetDrawable(Resource.Drawable.backButton));
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            Graficar = FindViewById<Button>(Resource.Id.button1);

            //Filtros

            CodCliente = FindViewById<Spinner>(Resource.Id.spinner1);
            IdPuntoFacturacion = FindViewById<Spinner>(Resource.Id.spinner2);
            Moneda = FindViewById<Spinner>(Resource.Id.spinner3);
            mesD = FindViewById<Spinner>(Resource.Id.spinner4);
            anioD = FindViewById<Spinner>(Resource.Id.spinner5);
            mesH = FindViewById<Spinner>(Resource.Id.spinner6);
            anioH = FindViewById<Spinner>(Resource.Id.spinner7);

            CodCliente.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(CodCliente_ItemSelected);
            IdPuntoFacturacion.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(IdPuntoFacturacion_ItemSelected);
            Moneda.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(Moneda_ItemSelected);
            mesD.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(MesD_ItemSelected);
            anioD.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(AnioD_ItemSelected);
            mesH.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(MesH_ItemSelected);
            anioH.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(AnioH_ItemSelected);

            ListarClientes();
            ListarMoneda();
            _Anio();
            ListarMesD();
            ListarAnioD();
            ListarMesH();
            ListarAnioH();

            Graficar.Click += (sender, e) => Grafic();

            void Grafic()
            {
                if (!string.IsNullOrEmpty(_codCliente) && !string.IsNullOrEmpty(_codPuntoF) && _codPuntoF != "0" &&
                    !string.IsNullOrEmpty(_mesD) && !string.IsNullOrEmpty(_anioD) && !string.IsNullOrEmpty(_mesH) &&
                    !string.IsNullOrEmpty(_anioH) && !string.IsNullOrEmpty(_moneda))
                {
                    //progress = ProgressDialog.Show(this, "", "Cargando...", true, false);
                    //progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await Graficos());
                    })).Start();
                }
                else if (string.IsNullOrEmpty(_codCliente))
                {
                    string toast = "Seleccione un cliente";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();

                }
                else if (string.IsNullOrEmpty(_codPuntoF) || _codPuntoF == "0")
                {
                    string toast = "Seleccione un punto de facturación";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else if (string.IsNullOrEmpty(_mesD))
                {
                    string toast = "Seleccione el mes inicial";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else if (string.IsNullOrEmpty(_anioD))
                {
                    string toast = "Seleccione el año inicial";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else if (string.IsNullOrEmpty(_mesH))
                {
                    string toast = "Seleccione el mes final";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else if (string.IsNullOrEmpty(_anioH))
                {
                    string toast = "Seleccione el año final";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else if (string.IsNullOrEmpty(_moneda))
                {
                    string toast = "Seleccione el tipo de moneda";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
            }
            async Task Graficos()
            {
                Graficar.Enabled = false;
                _fechaD = string.Format("01/{0}/{1}", _mesD, _anioD);
                _fechaH = string.Format("01/{0}/{1}", _mesH, _anioH);

                var IsConnected = await util.IsConnected();
                if (IsConnected == true)
                {
                    var intent_data = new Intent(this, typeof(HistPrecio_G));
                    intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                    intent_data.PutExtra("filtro", filtro);
                    intent_data.PutExtra("_codCliente", _codCliente);
                    intent_data.PutExtra("_codPuntoF", _codPuntoF);
                    intent_data.PutExtra("_moneda", _moneda);
                    intent_data.PutExtra("_fechaD", _fechaD);
                    intent_data.PutExtra("_fechaH", _fechaH);
                    intent_data.PutExtra("_modooff", "0");
                    StartActivity(intent_data);
                    Graficar.Enabled = true;
                }
                else if (IsConnected == false)
                {
                    if (_modooff != "0")
                    {
                        var intent_data = new Intent(this, typeof(HistPrecio_G));
                        intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                        intent_data.PutExtra("filtro", filtro);
                        intent_data.PutExtra("_codCliente", _codCliente);
                        intent_data.PutExtra("_codPuntoF", _codPuntoF);
                        intent_data.PutExtra("_moneda", _moneda);
                        intent_data.PutExtra("_fechaD", _fechaD);
                        intent_data.PutExtra("_fechaH", _fechaH);
                        intent_data.PutExtra("_modooff", _modooff);
                        StartActivity(intent_data);
                        Graficar.Enabled = true;
                        //progress.Dismiss();
                    }
                    else
                    {
                        Toast.MakeText(this, "En estos momentos no tiene servicio de internet ni datos sincronizados en Modo OffLine, por favor verificar.", ToastLength.Long).Show();
                        Graficar.Enabled = true;
                        //progress.Dismiss();
                    }
                }
            }
            void ListarMoneda()
            {
                string[] items = new[] { "USD", "PEN" };
                var Adaptador = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, items);
                Moneda.Adapter = Adaptador;
            }
            void Moneda_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                Spinner spinner = (Spinner)sender;
                _moneda = spinner.GetItemAtPosition(e.Position).ToString();
            }
            void ListarClientes()
            {
                _codPuntoF = null;
                var _clientes = clientes;
                adapter_c = new ClientesAdapter(this, clientes);
                CodCliente.Adapter = adapter_c;
            }

            void CodCliente_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codCliente = clientes[e.Position].CodCliente;
                puntoFacturacion.Clear();
                ListarPuntoFacturacion(_codCliente);
            }
            void ListarPuntoFacturacion(string _codCliente)
            {
                var _puntoFacturacion = clientes.Where(x => x.CodCliente == _codCliente).Select(x => x.PFaCliente).ToList();
                foreach (JavaList<BEPuntoFacturacion> item in _puntoFacturacion)
                {
                    for (var i = 0; i < item.Count; i++)
                    {
                        BEPuntoFacturacion punt = new BEPuntoFacturacion();
                        if (item[i].ID == "0")
                        {
                            punt.CodCliente = item[i].CodCliente;
                            punt.ID = item[i].ID;
                            punt.Descripcion = "SELECCIONE";
                        }
                        else
                        {
                            punt.CodCliente = item[i].CodCliente;
                            punt.ID = item[i].ID;
                            punt.Descripcion = item[i].Descripcion;
                        }
                        puntoFacturacion.Add(punt);
                    }
                }
                adapter_p = new PuntoFacturacionAdapter(this, puntoFacturacion);
                IdPuntoFacturacion.Adapter = adapter_p;
            }
            void IdPuntoFacturacion_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codPuntoF = puntoFacturacion[e.Position].ID;
            }
            void ListarMesD()
            {
                mes = COMes.ListarMes();
                adapter_m = new MesAdapter(this, mes);
                mesD.Adapter = adapter_m;
                var i = ((DateTime.Now).Month) - 1;
                mesD.SetSelection(i);
            }
            void MesD_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                var _mesDesde = Convert.ToInt32(mes[e.Position].IdMes);
                if (!string.IsNullOrEmpty(_anioD) && !string.IsNullOrEmpty(_mesD) && !string.IsNullOrEmpty(_anioD))
                {
                    var _anioDesde = Convert.ToInt32(_anioD);
                    var _mesHasta = Convert.ToInt32(_mesH);
                    var _anioHasta = Convert.ToInt32(_anioH);
                    if (_anioHasta > _anioDesde)
                    {
                        _mesD = mes[e.Position].IdMes;
                    }
                    else if (_anioHasta == _anioDesde)
                    {
                        if (_mesHasta >= _mesDesde)
                        {
                            _mesD = mes[e.Position].IdMes;
                        }
                        else
                        {
                            var i = Convert.ToInt32(_mesD) - 1;
                            mesD.SetSelection(i);
                            string toast = "El mes inicial es inválido";
                            Toast.MakeText(this, toast, ToastLength.Short).Show();
                        }
                    }
                }
                else
                {
                    _mesD = mes[e.Position].IdMes;
                }
            }
            void _Anio()
            {
                date = DateTime.Now;
                _array = date.Year - 2014;
                items = new string[_array];
                string anio = null;
                int _i = 0;
                for (int i = 2015; i <= date.Year; i++)
                {
                    anio = i.ToString();
                    items[_i] = anio;
                    _i = _i + 1;
                }
            }
            void ListarAnioD()
            {
                var Adaptador = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, items);
                anioD.Adapter = Adaptador;
                anioD.SetSelection(_array - 1);
            }
            void AnioD_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                Spinner spinner = (Spinner)sender;
                var _anioDesde = Convert.ToInt32(spinner.GetItemAtPosition(e.Position).ToString());
                if (!string.IsNullOrEmpty(_mesD) && !string.IsNullOrEmpty(_mesH) && !string.IsNullOrEmpty(_anioH))
                {
                    var _mesDesde = Convert.ToInt32(_mesD);
                    var _mesHasta = Convert.ToInt32(_mesH);
                    var _anioHasta = Convert.ToInt32(_anioH);
                    if (_anioHasta > _anioDesde)
                    {
                        _anioD = spinner.GetItemAtPosition(e.Position).ToString();
                    }
                    else if (_anioHasta == _anioDesde)
                    {
                        if (_mesHasta >= _mesDesde)
                        {
                            _anioD = spinner.GetItemAtPosition(e.Position).ToString();
                        }
                        else
                        {
                            var i = Convert.ToInt32(_anioD) - 2015;
                            anioD.SetSelection(i);
                            string toast = "El año incial es inválido";
                            Toast.MakeText(this, toast, ToastLength.Short).Show();
                        }
                    }
                    else
                    {
                        var i = Convert.ToInt32(_anioD) - 2015;
                        anioD.SetSelection(i);
                        string toast = "El año incial es inválido";
                        Toast.MakeText(this, toast, ToastLength.Short).Show();

                    }
                }
                else
                {
                    _anioD = spinner.GetItemAtPosition(e.Position).ToString();
                }
            }
            void ListarMesH()
            {
                mes = COMes.ListarMes();
                adapter_m = new MesAdapter(this, mes);
                mesH.Adapter = adapter_m;
                var i = ((DateTime.Now).Month) - 1;
                mesH.SetSelection(i);
            }
            void MesH_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                var _mesHasta = Convert.ToInt32(mes[e.Position].IdMes);
                if (!string.IsNullOrEmpty(_mesD) && !string.IsNullOrEmpty(_anioD) && !string.IsNullOrEmpty(_anioH))
                {
                    var _mesDesde = Convert.ToInt32(_mesD);
                    var _anioDesde = Convert.ToInt32(_anioD);
                    var _anioHasta = Convert.ToInt32(_anioH);
                    if (_anioHasta > _anioDesde)
                    {
                        _mesH = mes[e.Position].IdMes;
                    }
                    else if (_anioHasta == _anioDesde)
                    {
                        if (_mesHasta >= _mesDesde)
                        {
                            _mesH = mes[e.Position].IdMes;
                        }
                        else
                        {
                            var i = Convert.ToInt32(_mesH) - 1;
                            mesH.SetSelection(i);
                            string toast = "El mes final es inválido";
                            Toast.MakeText(this, toast, ToastLength.Short).Show();
                        }
                    }
                }
                else
                {
                    _mesH = mes[e.Position].IdMes;
                }
            }
            void ListarAnioH()
            {
                var Adaptador = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, items);
                anioH.Adapter = Adaptador;
                anioH.SetSelection(_array - 1);
            }
            void AnioH_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                Spinner spinner = (Spinner)sender;
                var _anioHasta = Convert.ToInt32(spinner.GetItemAtPosition(e.Position).ToString());
                if (!string.IsNullOrEmpty(_mesD) && !string.IsNullOrEmpty(_anioD) && !string.IsNullOrEmpty(_mesH))
                {
                    var _mesDesde = Convert.ToInt32(_mesD);
                    var _anioDesde = Convert.ToInt32(_anioD);
                    var _mesHasta = Convert.ToInt32(_mesH);
                    if (_anioHasta > _anioDesde)
                    {
                        _anioH = spinner.GetItemAtPosition(e.Position).ToString();
                    }
                    else if (_anioHasta == _anioDesde)
                    {
                        if (_mesHasta >= _mesDesde)
                        {
                            _anioH = spinner.GetItemAtPosition(e.Position).ToString();
                        }
                        else
                        {
                            var i = Convert.ToInt32(_anioH) - 2015;
                            anioH.SetSelection(i);
                            string toast = "El año final es inválido";
                            Toast.MakeText(this, toast, ToastLength.Short).Show();
                        }
                    }
                    else
                    {
                        var i = Convert.ToInt32(_anioH) - 2015;
                        anioH.SetSelection(i);
                        string toast = "El año final es inválido";
                        Toast.MakeText(this, toast, ToastLength.Short).Show();

                    }
                }
                else
                {
                    _anioH = spinner.GetItemAtPosition(e.Position).ToString();
                }
            }


        }

        //BackButton
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home) ;
            {
                Finish();
                //var intent_data = new Intent(this, typeof(PreConsActivity));
                //intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                //intent_data.PutExtra("filtro", filtro);
                //StartActivityFromFragment(intent_data);
            }
            return base.OnOptionsItemSelected(item);
        }

        //private void StartActivityFromFragment(Intent intent_data)
        //{
        //    SetContentView(layoutResID: Resource.Layout.prefac);
        //}
    }
}