﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SciChart.Charting.Visuals.RenderableSeries;
using SciChart.Charting.Model.DataSeries;
using SciChart.Drawing.Common;
using SciChart.Charting.Visuals.PointMarkers;
using SciChart.Charting.Visuals.Axes;
using E7.Droid.Utiles;

namespace E7.Droid.Consumo
{
    class ChartTypeModelFactory
    {
        //Reporte de Consumo - Consumo Facturado
        public static ChartTypeModel EnergiaActivaStackedColumn(double[] EnergiaActiva, double[] Fecha, uint fillColor, uint strokeColor)
        {
            try
            {
                var collection = new HorizontallyStackedColumnsCollection();

                var dataSeries = new XyDataSeries<double, double> { SeriesName = "Energía Activa" };
                for (var i = 0; i < EnergiaActiva.Count(); i++)
                {
                    dataSeries.Append(Fecha[i], EnergiaActiva[i]);
                }

                var rSeries = new StackedColumnRenderableSeries
                {
                    DataSeries = dataSeries,
                    StrokeStyle = new SolidPenStyle(fillColor, 6),
                    FillBrushStyle = new LinearGradientBrushStyle(0, 0, 0, 0, fillColor, strokeColor)
                };
                collection.Add(rSeries);

                var name = "ENERGÍA ACTIVA";
                var title = "Energía Activa (kWh)";
                var medida = "(kWh)";
                return new ChartTypeModel(collection, name, title, medida);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static ChartTypeModel PotenciaStackedLine(double[] Potencia, double[] Fecha, uint fillColor)
        {
            try
            {
                var collection = new HorizontallyStackedColumnsCollection();

                var dataSeries = new XyDataSeries<double, double> { SeriesName = "Potencia" };
                for (var i = 0; i < Potencia.Count(); i++)
                {
                    dataSeries.Append(Fecha[i], Potencia[i]);
                }

                var rSeries = new StackedColumnRenderableSeries
                {
                    DataSeries = dataSeries,
                    StrokeStyle = new SolidPenStyle(fillColor, 6),
                    FillBrushStyle = new LinearGradientBrushStyle(0, 0, 0, 0, fillColor, fillColor)
                };
                collection.Add(rSeries);

                var name = "POTENCIA";
                var title = "Potencia (kW)";
                var medida = "(kW)";
                return new ChartTypeModel(collection, name, title, medida);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //Reporte de Consumo - Medición y Grafica
        public static ChartTypeModel EnergiaActivaStackedMountains(Context context, double[] EnergiaActiva, DateTime[] FechayHora, bool isOneHundredPercent, uint fillColor, uint strokeColor)
        {
            try
            {
                var collection = new VerticallyStackedMountainsCollection { IsOneHundredPercent = isOneHundredPercent };

                var dataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Energía Activa" };
                for (var i = 0; i < EnergiaActiva.Count(); i++)
                {
                    dataSeries.Append(FechayHora[i], EnergiaActiva[i]);
                }
                var rSeries = new StackedMountainRenderableSeries
                {
                    DataSeries = dataSeries,
                    StrokeStyle = new SolidPenStyle(fillColor, 5),
                    AreaStyle = new LinearGradientBrushStyle(0, 0, 0, 1, fillColor, strokeColor),
                    PointMarker = new SquarePointMarker
                    {
                        Width = 10,
                        Height = 10,
                        FillStyle = new SolidBrushStyle(fillColor),
                        StrokeStyle = new SolidPenStyle(fillColor)
                    }
                };
                collection.Add(rSeries);


                var name = "ENERGÍA ACTIVA";
                var title = "Energía Activa (kWh)";
                var medida = "(kWh)";
                return new ChartTypeModel(collection, name, title, medida);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static ChartTypeModel PotenciaStackedMountains(Context context, double[] Potencia, DateTime[] FechayHora, bool isOneHundredPercent, uint fillColor, uint strokeColor)
        {
            try
            {
                var collection = new VerticallyStackedMountainsCollection { IsOneHundredPercent = isOneHundredPercent };

                var dataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Potencia" };
                for (var i = 0; i < Potencia.Count(); i++)
                {
                    dataSeries.Append(FechayHora[i], Potencia[i]);
                }

                var rSeries = new StackedMountainRenderableSeries
                {
                    DataSeries = dataSeries,
                    StrokeStyle = new SolidPenStyle(fillColor, 5),
                    AreaStyle = new LinearGradientBrushStyle(1, 1, 0, 1, 0, 0),
                    PointMarker = new SquarePointMarker
                    {
                        Width = 10,
                        Height = 10,
                        FillStyle = new SolidBrushStyle(fillColor),
                        StrokeStyle = new SolidPenStyle(fillColor)
                    }
                };
                collection.Add(rSeries);


                var name = "POTENCIA";
                var title = "Potencia (kW)";
                var medida = "(kW)";
                return new ChartTypeModel(collection, name, title, medida);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static ChartTypeModel EnergiaReactivaStackedMountains(Context context, double[] EnergiaReactiva, DateTime[] FechayHora, bool isOneHundredPercent, uint fillColor, uint strokeColor)
        {
            try
            {
                var collection = new VerticallyStackedMountainsCollection { IsOneHundredPercent = isOneHundredPercent };

                var dataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Energía Reactiva" };
                for (var i = 0; i < EnergiaReactiva.Count(); i++)
                {
                    dataSeries.Append(FechayHora[i], EnergiaReactiva[i]);
                }

                var rSeries = new StackedMountainRenderableSeries
                {
                    DataSeries = dataSeries,
                    StrokeStyle = new SolidPenStyle(fillColor, 5),
                    AreaStyle = new LinearGradientBrushStyle(0, 0, 0, 1, fillColor, strokeColor),
                    PointMarker = new SquarePointMarker
                    {
                        Width = 10,
                        Height = 10,
                        FillStyle = new SolidBrushStyle(fillColor),
                        StrokeStyle = new SolidPenStyle(fillColor)
                    }

                };
                collection.Add(rSeries);

                var name = "ENERGÍA REACTIVA";
                var title = "Energía Reactiva (kVarh)";
                var medida = "(kVarh)";
                return new ChartTypeModel(collection, name, title, medida);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public static ChartTypeModel TensionStackedMountains(Context context, double[] Tension, DateTime[] FechayHora, bool isOneHundredPercent, uint fillColor, uint strokeColor)
        {
            try
            {
                var collection = new VerticallyStackedMountainsCollection { IsOneHundredPercent = isOneHundredPercent };

                var dataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Tensión" };
                for (var i = 0; i < Tension.Count(); i++)
                {
                    dataSeries.Append(FechayHora[i], Tension[i]);
                }

                var rSeries = new StackedMountainRenderableSeries
                {
                    DataSeries = dataSeries,
                    StrokeStyle = new SolidPenStyle(fillColor, 5),
                    AreaStyle = new LinearGradientBrushStyle(0, 0, 0, 1, fillColor, strokeColor),
                    PointMarker = new SquarePointMarker
                    {   
                        Width = 10,
                        Height = 10,
                        FillStyle = new SolidBrushStyle(fillColor),
                        StrokeStyle = new SolidPenStyle(fillColor)
                    }

                };
                collection.Add(rSeries);

                var name = "TENSIÓN";
                var title = "Tensión (V)";
                var medida = "(V)";
                return new ChartTypeModel(collection, name, title, medida);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static ChartTypeModel CorrienteStackedMountains(Context context, double[] Corriente, DateTime[] FechayHora, bool isOneHundredPercent, uint fillColor, uint strokeColor)
        {
            try
            {
                var collection = new VerticallyStackedMountainsCollection { IsOneHundredPercent = isOneHundredPercent };

                var dataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Corriente" };
                for (var i = 0; i < Corriente.Count(); i++)
                {
                    dataSeries.Append(FechayHora[i], Corriente[i]);
                }

                var rSeries = new StackedMountainRenderableSeries
                {
                    DataSeries = dataSeries,
                    StrokeStyle = new SolidPenStyle(fillColor, 5),
                    AreaStyle = new LinearGradientBrushStyle(0, 0, 0, 1, fillColor, strokeColor),
                    PointMarker = new SquarePointMarker
                    {
                        Width = 10,
                        Height = 10,
                        FillStyle = new SolidBrushStyle(fillColor),
                        StrokeStyle = new SolidPenStyle(fillColor)
                    }

                };
                collection.Add(rSeries);

                var name = "CORRIENTE";
                var title = "Corriente (A)";
                var medida = "(A)";
                return new ChartTypeModel(collection, name, title, medida);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}