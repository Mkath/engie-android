﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using E7.Entidades;
using System.Threading;
using E7.Servicio;
using E7.Droid.Database;
using System.Threading.Tasks;
using E7.Droid.Utiles;
using ML;
using E7.Util;

namespace E7.Droid.Consumo
{
    [Activity(Label = "", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class ConsFacturado_F : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        ClientesAdapter adapter_c;
        PuntoFacturacionAdapter adapter_p;
        JavaList<BEClientes> clientes;
        JavaList<BEPuntoFacturacion> puntoFacturacion = new JavaList<BEPuntoFacturacion>();
        Spinner CodCliente, Anio, IdPuntoFacturacion;
        Button Graficar;
        string _usuarioInterno, _codCliente, _codPuntoF, _anio, filtro, _modooff;
        DateTime date;
        int _array;
        string[] items;
        DAReporte_Consumo conexion = new DAReporte_Consumo();
        BLTBLConsumoFacturado BLcf = new BLTBLConsumoFacturado();
        UTComunes util = new UTComunes();
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Layout.ConsFacturado_F);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(GetDrawable(Resource.Drawable.backButton));
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            //Listar Clientes y Punto de Facturacion
            _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            filtro = Intent.GetStringExtra("filtro") ?? "Data not available";
            ISharedPreferences pref = Application.Context.GetSharedPreferences("UserInfo", FileCreationMode.Private);
            _modooff = pref.GetString("_modooff", "0");

            clientes = (JavaList<BEClientes>)Newtonsoft.Json.JsonConvert.DeserializeObject<JavaList<BEClientes>>(filtro);


            CodCliente = FindViewById<Spinner>(Resource.Id.spinner1);
            Anio = FindViewById<Spinner>(Resource.Id.spinner2);
            IdPuntoFacturacion = FindViewById<Spinner>(Resource.Id.spinner3);
            Graficar = FindViewById<Button>(Resource.Id.button1);

            CodCliente.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(CodCliente_ItemSelected);
            Anio.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(Anio_ItemSelected);
            IdPuntoFacturacion.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(IdPuntoFacturacion_ItemSelected);

            ListarClientes();
            _Anio();
            ListarAnio();

            Graficar.Click += (sender, e) => Grafic();

            void Grafic()
            {
                if (!string.IsNullOrEmpty(_codCliente) && !string.IsNullOrEmpty(_anio) && !string.IsNullOrEmpty(_codPuntoF))
                {
                    //progress = ProgressDialog.Show(this, "", "Cargando...", true, false);
                    //progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await Graficos());
                    })).Start();
                }
                else if (string.IsNullOrEmpty(_codCliente))
                {
                    string toast = "Seleccione un cliente";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else if (string.IsNullOrEmpty(_anio))
                {
                    string toast = "Seleccione el año";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else
                {
                    string toast = "Seleccione un punto de facturación";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
            }
            async Task Graficos()
            {
                Graficar.Enabled = false;
                var IsConnected = await util.IsConnected();
                if (IsConnected == true)
                {
                    var intent_data = new Intent(this, typeof(ConsFacturado_G));
                    intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                    intent_data.PutExtra("_anio", _anio);
                    intent_data.PutExtra("_codCliente", _codCliente);
                    intent_data.PutExtra("_codPuntoF", _codPuntoF);
                    intent_data.PutExtra("filtro", filtro);
                    intent_data.PutExtra("_modooff", "0");
                    StartActivity(intent_data);
                    Graficar.Enabled = true;
                    //progress.Dismiss();
                }
                else if (IsConnected == false)
                {
                    if (_modooff != "0")
                    {
                        var intent_data = new Intent(this, typeof(ConsFacturado_G));
                        intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                        intent_data.PutExtra("_anio", _anio);
                        intent_data.PutExtra("_codCliente", _codCliente);
                        intent_data.PutExtra("_codPuntoF", _codPuntoF);
                        intent_data.PutExtra("filtro", filtro);
                        intent_data.PutExtra("_modooff", _modooff);
                        StartActivity(intent_data);
                        Graficar.Enabled = true;
                    }
                    else
                    {
                        Toast.MakeText(this, "En estos momentos no tiene servicio de internet ni datos sincronizados en Modo OffLine, por favor verificar.", ToastLength.Long).Show();
                        Graficar.Enabled = true;
                    }
                }
            }

            //Cargar Data Cliente
            void ListarClientes()
            {
                _codPuntoF = null;
                var _clientes = clientes;
                adapter_c = new ClientesAdapter(this, clientes);
                CodCliente.Adapter = adapter_c;
            }

            void CodCliente_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codCliente = clientes[e.Position].CodCliente;
                ListarPuntoFacturacion(_codCliente);
            }
            void _Anio()
            {
                date = DateTime.Now;
                _array = date.Year - 2014;
                items = new string[_array];
                string anio = null;
                int _i = 0;
                for (int i = 2015; i <= date.Year; i++)
                {
                    anio = i.ToString();
                    items[_i] = anio;
                    _i = _i + 1;
                }
            }
            void ListarAnio()
            {
                var Adaptador = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, items);
                Anio.Adapter = Adaptador;
                Anio.SetSelection(_array - 1);
            }

            void Anio_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                Spinner spinner = (Spinner)sender;
                _anio = spinner.GetItemAtPosition(e.Position).ToString();
            }
            void ListarPuntoFacturacion(string _codCliente)
            {
                var _puntoFacturacion = clientes.Where(x => x.CodCliente == _codCliente).Select(x => x.PFaCliente).ToList();

                foreach (JavaList<BEPuntoFacturacion> item in _puntoFacturacion)
                {
                    puntoFacturacion = item;
                }

                adapter_p = new PuntoFacturacionAdapter(this, puntoFacturacion);
                IdPuntoFacturacion.Adapter = adapter_p;
            }
            void IdPuntoFacturacion_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codPuntoF = puntoFacturacion[e.Position].ID;
            }
        }

        //BackButton
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home) ;
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}