﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using E7.Entidades;
using System.Threading;
using E7.Servicio;
using E7.Droid.Database;
using System.Threading.Tasks;
using System.Globalization;
using E7.Droid.Utiles;
using ML;
using E7.Util;

namespace E7.Droid.Consumo
{
    [Activity(Label = "", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MediGrafica_F : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        ClientesAdapter adapter_c;
        MedidorAdapter adapter_m;
        JavaList<BEClientes> clientes;
        JavaList<BEMedidor> medidores;
        Spinner CodCliente, CodMedidor;
        ImageView _dateSelectButton, _dateSelectButton1;
        Button Graficar;
        Android.App.ProgressDialog progress;
        string _usuarioInterno, _codCliente, _codMedidor, _fechaD, _fechaH, filtro, dt, dt1, _modooff;
        Thread thread = null;
        TextView _dateDisplay, _dateDisplay1;
        DateTime _dt, _dt1, _dte, _dte1;
        DAReporte_Consumo conexion = new DAReporte_Consumo();
        DATBLMedicionGrafica DAmg = new DATBLMedicionGrafica();
        UTComunes util = new UTComunes();
        protected override void OnCreate(Bundle bundle)   
        {
            base.OnCreate(bundle);

            // Create your application here

            //Listar Clientes y Medidor
            _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            filtro = Intent.GetStringExtra("filtro") ?? "Data not available";
            ISharedPreferences pref = Application.Context.GetSharedPreferences("UserInfo", FileCreationMode.Private);
            _modooff = pref.GetString("_modooff", "0");

            clientes = (JavaList<BEClientes>)Newtonsoft.Json.JsonConvert.DeserializeObject<JavaList<BEClientes>>(filtro);

            SetContentView(layoutResID: Resource.Layout.MediGrafica_F);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(GetDrawable(Resource.Drawable.backButton));
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);


            _dateDisplay = FindViewById<TextView>(Resource.Id.textView6);
            _dateSelectButton = FindViewById<ImageView>(Resource.Id.imageView4);
            _dateDisplay1 = FindViewById<TextView>(Resource.Id.textView7);
            _dateSelectButton1 = FindViewById<ImageView>(Resource.Id.imageView5);
            _dateSelectButton.Click += DateSelect_OnClick;
            _dateSelectButton1.Click += DateSelect1_OnClick;
            _dateDisplay.Click += DateSelect_OnClick;
            _dateDisplay1.Click += DateSelect1_OnClick;

            CodCliente = FindViewById<Spinner>(Resource.Id.spinner1);
            CodMedidor = FindViewById<Spinner>(Resource.Id.spinner2);
            Graficar = FindViewById<Button>(Resource.Id.button1);

            CodCliente.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(CodCliente_ItemSelected);
            CodMedidor.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(CodMedidor_ItemSelected);
            
            ListarClientes();
            Fecha();

            Graficar.Click += (sender, e) => Grafic();

            void Grafic()
            {
                _fechaD = _dateDisplay.Text;
                _fechaH = _dateDisplay1.Text;
                if (!string.IsNullOrEmpty(_codCliente) && !string.IsNullOrEmpty(_codMedidor) && !string.IsNullOrEmpty(_fechaD) && !string.IsNullOrEmpty(_fechaH))
                {
                    //progress = ProgressDialog.Show(this, "", "Cargando...", true, false);
                    //progress.SetProgressStyle(ProgressDialogStyle.Spinner);

                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await Graficos());

                    })).Start();
                }
                else if (string.IsNullOrEmpty(_codCliente))
                {
                    string toast = "Seleccione el cliente";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else if (string.IsNullOrEmpty(_codMedidor))
                {
                    string toast = "Seleccione el medidor";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else if (string.IsNullOrEmpty(_fechaD))
                {
                    string toast = "Seleccione la fecha Desde";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else
                {
                    string toast = "Seleccione la fecha Hasta";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
            }
            async Task Graficos()
            {
                Graficar.Enabled = false;
                var IsConnected = await util.IsConnected();
                if (IsConnected == true)
                {
                    var intent_data = new Intent(this, typeof(MediGrafica_G));
                    intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                    intent_data.PutExtra("_fechaD", _fechaD);
                    intent_data.PutExtra("_fechaH", _fechaH);
                    intent_data.PutExtra("_codCliente", _codCliente);
                    intent_data.PutExtra("_codMedidor", _codMedidor);
                    intent_data.PutExtra("filtro", filtro);
                    intent_data.PutExtra("_modooff", "0");
                    StartActivity(intent_data);
                    Graficar.Enabled = true;
                    //progress.Dismiss();
                }
                else if (IsConnected == false)
                {
                    if (_modooff != "0")
                    {
                        var intent_data = new Intent(this, typeof(MediGrafica_G));
                        intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                        intent_data.PutExtra("_fechaD", _fechaD);
                        intent_data.PutExtra("_fechaH", _fechaH);
                        intent_data.PutExtra("_codCliente", _codCliente);
                        intent_data.PutExtra("_codMedidor", _codMedidor);
                        intent_data.PutExtra("filtro", filtro);
                        intent_data.PutExtra("_modooff", _modooff);
                        StartActivity(intent_data);
                        Graficar.Enabled = true;
                        //progress.Dismiss();
                    }
                    else
                    {
                        Toast.MakeText(this, "En estos momentos no tiene servicio de internet ni datos sincronizados en Modo OffLine, por favor verificar.", ToastLength.Long).Show();
                        Graficar.Enabled = true;
                    }
                    //progress.Dismiss();
                }
            }

            void Fecha()
            {
                var time = DateTime.Now;
                _dateDisplay.Text = String.Format("01/{0}/{1}", (time.Month.ToString().Length == 1) ? "0" + time.Month.ToString() : time.Month.ToString(), time.Year);
                _dateDisplay1.Text = time.ToString("dd/MM/yyyy");
            }
            void ListarClientes()
            {
                _codMedidor = null;
                var _clientes = clientes;
                adapter_c = new ClientesAdapter(this, clientes);
                CodCliente.Adapter = adapter_c;
            }

            void CodCliente_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codCliente = clientes[e.Position].CodCliente;
                ListarMedidor(_codCliente);

            }
            void ListarMedidor(string _codCliente)
            {
                var _medidores = clientes.Where(x => x.CodCliente == _codCliente).Select(x => x.MedCliente).ToList();

                foreach (JavaList<BEMedidor> item in _medidores)
                {
                    medidores = item;
                }
                adapter_m = new MedidorAdapter(this, medidores);
                CodMedidor.Adapter = adapter_m;
            }
            void CodMedidor_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codMedidor = medidores[e.Position].Codigo;
            }
            void DateSelect_OnClick(object sender, EventArgs eventArgs)
            {
                _dt = DateTime.ParseExact(_dateDisplay.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                _dt1 = DateTime.ParseExact(_dateDisplay1.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
                {
                    dt = time.ToString("dd/MM/yyyy");
                    if (!string.IsNullOrEmpty(dt) && (!string.IsNullOrEmpty(_dateDisplay1.Text)))
                    {
                        //_dte = Convert.ToDateTime(dt);
                        _dte = time;

                        if (_dt1 >= _dte)
                        {
                            _dateDisplay.Text = dt;
                        }
                        else
                        {
                            _dateDisplay.Text = _dt.ToString("dd/MM/yyyy"); ;
                            string toast = "La fecha inicial es inválida";
                            Toast.MakeText(this, toast, ToastLength.Short).Show();
                        }
                    }
                    else if (!string.IsNullOrEmpty(_dateDisplay1.Text))
                    {
                        _dateDisplay.Text = dt;
                    }
                    else
                    {
                        string toast = "La fecha inicial es inválida";
                        Toast.MakeText(this, toast, ToastLength.Short).Show();
                    }
                });
                frag.Show(FragmentManager, DatePickerFragment.TAG);


            }
            void DateSelect1_OnClick(object sender, EventArgs eventArgs)
            {
                _dt = DateTime.ParseExact(_dateDisplay.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                _dt1 = DateTime.ParseExact(_dateDisplay1.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
                {
                    dt1 = time.ToString("dd/MM/yyyy");
                    if (!string.IsNullOrEmpty(dt1) && (!string.IsNullOrEmpty(_dateDisplay.Text)))
                    {
                        //_dte = Convert.ToDateTime(dt);
                        _dte = time;

                        if (_dte >= _dt)
                        {
                            _dateDisplay1.Text = dt1;
                        }
                        else
                        {
                            _dateDisplay1.Text = _dt1.ToString("dd/MM/yyyy"); ;
                            string toast = "La fecha final es inválida";
                            Toast.MakeText(this, toast, ToastLength.Short).Show();
                        }
                    }
                    else if (!string.IsNullOrEmpty(_dateDisplay1.Text))
                    {
                        _dateDisplay1.Text = dt1;
                    }
                    else
                    {
                        string toast = "La fecha final es inválida";
                        Toast.MakeText(this, toast, ToastLength.Short).Show();
                    }
                });
                frag.Show(FragmentManager, DatePickerFragment.TAG);
            }
        }


        //BackButton
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home) ;
            {
                Finish();
                //var intent_data = new Intent(this, typeof(PreConsActivity));
                //intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                //intent_data.PutExtra("filtro", filtro);
                //StartActivityFromFragment(intent_data);
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}