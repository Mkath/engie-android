﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using System.Threading;
using Android.Support.V7.App;
using System.Threading.Tasks;
using E7.Entidades;
using E7.Servicio;
using E7.Droid.Database;
using System.Globalization;
using E7.Droid.Utiles;
using E7.Model;
using ML;

namespace E7.Droid.Consumo
{
    [Activity(Label = "", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MediGrafica_G : AppCompatActivity
    {
        string _usuarioInterno, filtro;
        ProgressDialog progress;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.MediGrafica_G);

            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(GetDrawable(Resource.Drawable.backButton));
            TabLayout TabLayout = FindViewById<TabLayout>(Resource.Id.tabLayout);
            ViewPager ViewPager = FindViewById<ViewPager>(Resource.Id.viewpager);
            TextView ToolOff = FindViewById<TextView>(Resource.Id.modooff); ;
            TextView ToolTitle = FindViewById<TextView>(Resource.Id.toolbar_title);
            IList<ChartTypeModel> _chartTypesSource = new List<ChartTypeModel>();

            //Obtener Datos
            _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "data not available";
            filtro = Intent.GetStringExtra("filtro") ?? "Data not available";
            string _fechaD = Intent.GetStringExtra("_fechaD") ?? "data not available";
            string _fechaH = Intent.GetStringExtra("_fechaH") ?? "data not available";
            string _codCliente = Intent.GetStringExtra("_codCliente") ?? "data not available";
            string _codMedidor = Intent.GetStringExtra("_codMedidor") ?? "data not available";
            string _modooff = Intent.GetStringExtra("_modooff") ?? "0";

            ModoOff();
            Inicializar();

            void ModoOff()
            {
                if (_modooff == "0")
                {
                    ToolOff.Visibility = ViewStates.Gone;
                    ToolTitle.LayoutParameters.Width = LinearLayout.LayoutParams.WrapContent;
                    ToolTitle.SetMinLines(1);
                    ToolTitle.SetMaxLines(1);
                }
                else
                {
                    ToolOff.Visibility = ViewStates.Visible;
                }
            }
            void Inicializar()
            {
                progress = new ProgressDialog(this);
                progress.Indeterminate = true;
                progress.SetProgressStyle(Android.App.ProgressDialogStyle.Spinner);
                progress.SetMessage("Obteniendo datos...");
                progress.SetCancelable(false);
                progress.Show();
                new Thread(new ThreadStart(delegate
                {
                    RunOnUiThread(async () => await CargarGrafico());

                })).Start();

            }

            async Task CargarGrafico()
            {
                List<BEMedicionGrafica> resultado;
                List<TBLMedicionGrafica> resultado1;
                DAReporte_Consumo conexion = new DAReporte_Consumo();
                BLTBLMedicionGrafica conexion1 = new BLTBLMedicionGrafica();
                TBLMedicionGrafica data = new TBLMedicionGrafica();
                string MensajeErrorMG;
                if (_modooff == "0")
                {
                    resultado = await conexion.Reporte_MedidionGrafica(_fechaD, _fechaH, _codCliente, _codMedidor);
                    MensajeErrorMG = conexion.ResultadoFinalRMG;
                    if (resultado == null)
                    {
                        progress.Dismiss();
                        Toast.MakeText(this, MensajeErrorMG, ToastLength.Long).Show();
                    }
                    else if (resultado.Count == 0)
                    {
                        progress.Dismiss();
                        Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                        alert.SetMessage("No hay datos.");
                        alert.SetCancelable(false);
                        alert.SetPositiveButton("OK", (senderAlert, args) =>
                        {
                            Finish();
                        });
                        alert.Show();
                    }
                    else
                    {
                        var EnergiaActiva = resultado.OrderBy(x => x.FechayHora).Select(x => x.EnergiaActiva).ToArray();
                        var Potencia = resultado.OrderBy(x => x.FechayHora).Select(x => x.Potencia).ToArray();
                        var EnergiaReactiva = resultado.OrderBy(x => x.FechayHora).Select(x => x.EnergiaReactiva).ToArray();
                        var Tension = resultado.OrderBy(x => x.FechayHora).Select(x => double.Parse((string.IsNullOrEmpty(x.Tension)) ? "0" : x.Tension)).ToArray();
                        var Corriente = resultado.OrderBy(x => x.FechayHora).Select(x => double.Parse((string.IsNullOrEmpty(x.Corriente)) ? "0" : x.Corriente)).ToArray();
                        var FechayHora = resultado.OrderBy(x => x.FechayHora).Select(x => x.FechayHora).ToArray();
                        GraficarMontain(EnergiaActiva, Potencia, EnergiaReactiva, Tension, Corriente, FechayHora);
                    }
                }
                else if (_modooff != "0")
                {
                    data.CodCliente = _codCliente;
                    data.CodMedicion = _codMedidor;
                    DateTime fechaD = DateTime.ParseExact(_fechaD, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime fechaH = DateTime.ParseExact(_fechaH, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    resultado1 = await conexion1.Reporte_MedicionGrafica(data);
                    MensajeErrorMG = "Error al momento de leer los datos guardados";
                    if (resultado1 == null)
                    {
                        progress.Dismiss();
                        Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                        alert.SetTitle("Modo OffLine");
                        alert.SetMessage("No hay datos sincronizados.");
                        alert.SetCancelable(false);
                        alert.SetPositiveButton("OK", (senderAlert, args) =>
                        {
                            Finish();
                        });
                        alert.Show();
                    }
                    else if (resultado1.Count == 0)
                    {
                        progress.Dismiss();
                        Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                        alert.SetTitle("Modo OffLine");
                        alert.SetMessage("No hay datos sincronizados.");
                        alert.SetCancelable(false);
                        alert.SetPositiveButton("OK", (senderAlert, args) =>
                        {
                            Finish();
                        });
                        alert.Show();
                    }
                    else
                    {
                        var list = Encoding.UTF8.GetString(resultado1[0].BusquedaJson);
                        resultado = (List<BEMedicionGrafica>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEMedicionGrafica>>(list);

                        var _ultimoMes = resultado.Where(x => (x.FechayHora <= fechaD)).ToArray();
                        double[] EnergiaActiva = resultado.OrderBy(z => z.FechayHora).Where(y => y.FechayHora > fechaD && y.FechayHora < fechaH).Select(x => x.EnergiaActiva).ToArray();
                        double[] Potencia = resultado.OrderBy(z => z.FechayHora).Where(y => y.FechayHora > fechaD && y.FechayHora < fechaH).Select(x => x.Potencia).ToArray();
                        double[] EnergiaReactiva = resultado.OrderBy(z => z.FechayHora).Where(y => y.FechayHora > fechaD && y.FechayHora < fechaH).Select(x => x.EnergiaReactiva).ToArray();
                        double[] Tension = resultado.OrderBy(z => z.FechayHora).Where(y => y.FechayHora > fechaD && y.FechayHora < fechaH).Select(x => double.Parse((string.IsNullOrEmpty(x.Tension)) ? "0" : x.Tension)).ToArray();
                        double[] Corriente = resultado.OrderBy(z => z.FechayHora).Where(y => y.FechayHora > fechaD && y.FechayHora < fechaH).Select(x => double.Parse((string.IsNullOrEmpty(x.Corriente)) ? "0" : x.Corriente)).ToArray();
                        DateTime[] FechayHora = resultado.OrderBy(z => z.FechayHora).Where(y => y.FechayHora > fechaD && y.FechayHora < fechaH).Select(x => x.FechayHora).ToArray();

                        if (FechayHora.Count() > 0 && _ultimoMes.Count() > 0)
                        {
                            GraficarMontain(EnergiaActiva, Potencia, EnergiaReactiva, Tension, Corriente, FechayHora);
                        }
                        else
                        {
                            string _meses = "";
                            if (_modooff == "1") { _meses = " mes."; } else { _meses = " meses."; }
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                            alert.SetTitle("Modo OffLine");
                            alert.SetMessage("Usted solo tiene datos sincronizados de " + _modooff + _meses);
                            alert.SetCancelable(false);
                            alert.SetPositiveButton("OK", (senderAlert, args) =>
                            {
                                EnergiaActiva = resultado.OrderBy(z => z.FechayHora).Select(x => x.EnergiaActiva).ToArray();
                                Potencia = resultado.OrderBy(z => z.FechayHora).Select(x => x.Potencia).ToArray();
                                EnergiaReactiva = resultado.OrderBy(z => z.FechayHora).Select(x => x.EnergiaReactiva).ToArray();
                                Tension = resultado.OrderBy(z => z.FechayHora).Select(x => double.Parse((string.IsNullOrEmpty(x.Tension)) ? "0" : x.Tension)).ToArray();
                                Corriente = resultado.OrderBy(z => z.FechayHora).Select(x => double.Parse((string.IsNullOrEmpty(x.Corriente)) ? "0" : x.Corriente)).ToArray();
                                FechayHora = resultado.OrderBy(z => z.FechayHora).Select(x => x.FechayHora).ToArray();
                                GraficarMontain(EnergiaActiva, Potencia, EnergiaReactiva, Tension, Corriente, FechayHora);
                            });
                            alert.Show();
                        }
                    }
                }

                void GraficarMontain(double[] EnergiaActiva, double[] Potencia, double[] EnergiaReactiva, double[] Tension, double[] Corriente, DateTime[] FechayHora)
                {
                    try
                    {
                        _chartTypesSource.Add(ChartTypeModelFactory.EnergiaActivaStackedMountains(this, EnergiaActiva, FechayHora, false, 0xff7cb5ec, 0xffffffff));
                        _chartTypesSource.Add(ChartTypeModelFactory.PotenciaStackedMountains(this, Potencia, FechayHora, false, 0xff7cb5ec, 0xffffffff));
                        _chartTypesSource.Add(ChartTypeModelFactory.EnergiaReactivaStackedMountains(this, EnergiaReactiva, FechayHora, false, 0xff7cb5ec, 0xffffffff));
                        _chartTypesSource.Add(ChartTypeModelFactory.TensionStackedMountains(this, Tension, FechayHora, false, 0xff7cb5ec, 0xffffffff));
                        _chartTypesSource.Add(ChartTypeModelFactory.CorrienteStackedMountains(this, Corriente, FechayHora, false, 0xff7cb5ec, 0xffffffff));

                        //this line fixes swiping lag of the viewPager by caching the pages
                        ViewPager.OffscreenPageLimit = 3;
                        ViewPager.Adapter = new ViewPagerCons2Adapter(BaseContext, _chartTypesSource);
                        TabLayout.SetupWithViewPager(ViewPager);
                        progress.Dismiss();
                    }
                    catch (Exception ex)
                    {
                        progress.Dismiss();
                        Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
                    }
                }
            }
        }

        //BackButton
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}