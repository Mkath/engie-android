﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using System.Threading;
using System.Threading.Tasks;
using E7.Droid.Database;
using E7.Entidades;
using E7.Droid.Usuario;
using E7.Util;
using ML;

namespace E7.Droid.OffLine
{
    [Activity(Label = "", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class Offline : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        Button btnSincronizar;
        Spinner spnMeses;
        TextView lblMes, FechaOff;
        Android.App.ProgressDialog progress, progresscf, progressmg, progressdf, progresshf, progresshp;
        string _usuarioInterno, _nombreCompleto, _filtro, _fechaoff;
        int _mes;
        List<BEClientes> filtro;
        BLTBLDistribucionFacturada blDF = new BLTBLDistribucionFacturada();
        BLTBLHistoricoFacturado blHF = new BLTBLHistoricoFacturado();
        BLTBLHistoricoPrecio blHP = new BLTBLHistoricoPrecio();
        BLTBLConsumoFacturado blCF = new BLTBLConsumoFacturado();
        BLTBLMedicionGrafica blMG = new BLTBLMedicionGrafica();
        UTComunes util = new UTComunes();
        Inicio inicioFragment;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(layoutResID: Resource.Layout.Offline);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(GetDrawable(Resource.Drawable.backButton));
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            spnMeses = FindViewById<Spinner>(Resource.Id.spinner1);
            lblMes = FindViewById<TextView>(Resource.Id.textView3);
            FechaOff = FindViewById<TextView>(Resource.Id.fechaoff);
            btnSincronizar = FindViewById<Button>(Resource.Id.button1);

            _nombreCompleto = Intent.GetStringExtra("_nombreCompleto") ?? "Data not available";
            _filtro = Intent.GetStringExtra("filtro") ?? "Data not available";
            ISharedPreferences pref = Application.Context.GetSharedPreferences("UserInfo", FileCreationMode.Private);
            _fechaoff = pref.GetString("_fechaoff", "0");

            filtro = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEClientes>>(_filtro);
            btnSincronizar.Click += (sender, e) => Sincronizar();
            _ListarMeses();
            Cargar();

            spnMeses.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(Meses_ItemSelected);

            void Cargar()
            {
                if (_fechaoff == "0")
                {
                    FechaOff.Text = "";
                }
                else {
                    FechaOff.Text = "Última fecha de sincronización: " + _fechaoff;
                }
            }

            void _ListarMeses()
            {
                string[] items = new[] { "1", "2", "3" };
                var Adaptador = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerItem, items);
                spnMeses.Adapter = Adaptador;
                Adaptador.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
                spnMeses.SetSelection(0);
            };

            void Meses_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                Spinner spinner = (Spinner)sender;
                _mes = Convert.ToInt32(spinner.GetItemAtPosition(e.Position).ToString());
                if (e.Position == 0)
                {
                    lblMes.Text = "mes";
                }
                else
                {
                    lblMes.Text = "meses";
                }
            }

            void Mensaje(string text, string title)
            {
                Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                alert.SetTitle(title);
                alert.SetMessage(text);
                alert.SetCancelable(false);
                alert.SetPositiveButton("OK", (senderAlert, args) =>
                {
                });
                alert.Show();
            }
            void Sincronizar()
            {
                new Thread(new ThreadStart(delegate
                {
                    RunOnUiThread(async () => await SincronizarTodo());
                })).Start();

            }
            async Task SincronizarTodo()
            {
                var IsConnected = await util.IsConnected();
                if (IsConnected == true)
                {
                    progress = ProgressDialog.Show(this, "", "Creando Tablas", true, false);
                    progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                    progress.Show();
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await CreateTableDF());
                    })).Start();
                }
                else
                {
                    Toast.MakeText(this, "En estos momentos no tiene servicio de internet, por favor verificar.", ToastLength.Long).Show();
                }
            }
            async Task CreateTableCF()
            {
                progress.SetMessage("Sincronizando datos 4 de 5");
                var salida = await blCF.CreateTableCF();
                if (salida == true || salida == false)
                {
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await SincronizarCF());
                    })).Start();
                }
            }
            async Task CreateTableMG()
            {
                progress.SetMessage("Sincronizando datos 5 de 5");
                var salida = await blMG.CreateTableMG();
                if (salida == true || salida == false)
                {
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await SincronizarMG());
                    })).Start();
                }
            }
            async Task CreateTableDF()
            {
                progress.SetMessage("Sincronizando datos 1 de 5");
                var salida = await blDF.CreateTableDF();
                if (salida == true || salida == false)
                {
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await SincronizarDF());
                    })).Start();
                }
            }
            async Task CreateTableHF()
            {
                progress.SetMessage("Sincronizando datos 2 de 5");
                var salida = await blHF.CreateTableHF();
                if (salida == true || salida == false)
                {
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await SincronizarHF());
                    })).Start();
                }
            }

            async Task CreateTableHP()
            {
                progress.SetMessage("Sincronizando datos 3 de 5");
                var salida = await blHP.CreateTableHP();
                if (salida == true || salida == false)
                {
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await SincronizarHP());
                    })).Start();
                }
            }
            async Task SincronizarCF()
            {
                var salida = await blCF.SincronizarCF(filtro);
                if (salida == true)
                {
                    //progresscf.Dismiss();
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await CreateTableMG());
                    })).Start();
                }
                else
                {
                    //progresscf.Dismiss();
                    progress.Dismiss();
                    Mensaje("Falló el proceso de sincronización 4/5", "Modo OffLine");
                }
            }
            async Task SincronizarMG()
            {
                var salida = await blMG.SincronizarMG(filtro, _mes);
                if (salida == true)
                {
                    //progressmg.Dismiss();
                    progress.Dismiss();
                    _fechaoff = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
                    //ISharedPreferences pref = Application.Context.GetSharedPreferences("UserInfo", FileCreationMode.Private);
                    ISharedPreferencesEditor edit = pref.Edit();
                    edit.PutString("_modooff", _mes.ToString());
                    edit.PutString("_fechaoff", _fechaoff);
                    edit.Apply();
                    Mensaje("Culminó el proceso de sincronización", "Modo OffLine");
                    Cargar();
                }
                else
                {
                    //progressmg.Dismiss();
                    progress.Dismiss();
                    Mensaje("Falló el proceso de sincronización 5/5", "Modo OffLine");
                }
            }
            async Task SincronizarDF()
            {
                var salida = await blDF.SincronizarDF(filtro, _mes);
                if (salida == true)
                {
                    //progressdf.Dismiss();
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await CreateTableHF());
                    })).Start();
                }
                else
                {
                    //progressdf.Dismiss();
                    progress.Dismiss();
                    Mensaje("Falló el proceso de sincronización 1/5", "Modo OffLine");
                }
            }
            async Task SincronizarHF()
            {
                var salida = await blHF.SincronizarHF(filtro, _mes);
                if (salida == true)
                {
                    //progresshf.Dismiss();
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await CreateTableHP());
                    })).Start();
                }
                else
                {
                    //progresshf.Dismiss();
                    progress.Dismiss();
                    Mensaje("Falló el proceso de sincronización 2/5", "Modo OffLine");
                }
            }
            async Task SincronizarHP()
            {
                var salida = await blHP.SincronizarHP(filtro, _mes);
                if (salida == true)
                {
                    //progresshp.Dismiss();
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await CreateTableCF());
                    })).Start();
                }
                else
                {
                    //progresshp.Dismiss();
                    progress.Dismiss();
                    Mensaje("Falló el proceso de sincronización 3/5", "Modo OffLine");
                }
            }
        }

        //BackButton
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
                var intent = new Intent(this, typeof(PrincipalFragment));
                intent.PutExtra("_nombreCompleto", _nombreCompleto);
                intent.PutExtra("filtro", _filtro);
                StartActivity(intent);
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}