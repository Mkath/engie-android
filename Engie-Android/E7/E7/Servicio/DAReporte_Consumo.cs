﻿using E7.Entidades;
using E7.Util;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace E7.Servicio
{
    public class DAReporte_Consumo
    {
        public string ResultadoFinalRCF { get; set; }
        public string ResultadoFinalRMG { get; set; }
        UTConfiguracion config = new UTConfiguracion();

        public Task<List<BEConsumoFacturado>> Reporte_ConsumoFacturado(string Anio, string CodCliente, string CodPuntoFacturacion)
        {
            #region Método Get Listar Consumo Facturado
            return Task.Run(() =>
            {
                try
                {
                    List<BEConsumoFacturado> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(config.URLServicesReporte);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteC_ConsumoFacturado?Anio=" + Anio + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEConsumoFacturado>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEConsumoFacturado>>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRCF = ex.Message;
                    return null;
                }
            });
            #endregion  
        }
        public Task<List<BEMedicionGrafica>> Reporte_MedidionGrafica(string FechaDesde, string FechaHasta, string CodCliente, string CodMedidor)
        {
            #region Método Get Listar Consumo Facturado
            return Task.Run(() =>
            {
                try
                {
                    List<BEMedicionGrafica> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(config.URLServicesReporte);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteC_MedicionyGrafica?FechaDesde=" + FechaDesde + "&FechaHasta=" + FechaHasta + "&CodCliente=" + CodCliente + "&CodMedidor=" + CodMedidor).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEMedicionGrafica>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEMedicionGrafica>>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRMG = ex.Message;
                    return null;
                }
            });
            #endregion  
        }
    }
}
