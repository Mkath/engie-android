﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E7.Model
{
    public class TBLConsumoFacturado
    {
        public string Anio { get; set; }
        public string CodCliente { get; set; }
        public string CodPuntoFacturacion { get; set; }
        public string EnergiaActiva { get; set; }
        public string Potencia { get; set; }
        public DateTime Fecha { get; set; }
    }
}