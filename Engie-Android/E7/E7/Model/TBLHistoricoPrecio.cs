﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E7.Model
{
    public class TBLHistoricoPrecio
    {
        public DateTime Fecha { get; set; }
        public string CodCliente { get; set; }
        public string CodPuntoFacturacion { get; set; }
        public string Moneda { get; set; }
        public string Concepto { get; set; }
        public string Consumo { get; set; }
    }
}