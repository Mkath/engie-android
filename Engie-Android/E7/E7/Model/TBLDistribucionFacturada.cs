﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E7.Model
{
    public class TBLDistribucionFacturada
    {
        public string Mes { get; set; }
        public string Anio { get; set; }
        public DateTime Fecha { get; set; }
        public string CodCliente { get; set; }
        public string CodPuntoFacturacion { get; set; }
        public string Concepto { get; set; }
        public string Consumo { get; set; }
    }
}