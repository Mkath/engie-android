using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using System.Net.Http;

namespace E7.Droid
{
    public class COLogin
    {
        public BEUsuarioLogin AutenticarUsuario(string Usuario, string Clave)
        {
            #region M�todo Get UsuarioLogin
            string Api = "http://appwebservice.engie-energia.pe/ServicioPrueba/Api/SLogin/";

            BEUsuarioLogin entidad = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Api);

                // Realizar la petici�n GET
                HttpResponseMessage response = client.GetAsync("AutenticarUsuario?Usuario=" + Usuario + "&Clave=" + Clave).Result;

                if (response.IsSuccessStatusCode)
                {
                    var res = response.Content.ReadAsStringAsync().Result;
                    entidad = (BEUsuarioLogin)Newtonsoft.Json.JsonConvert.DeserializeObject<BEUsuarioLogin>(res);
                }
            }
            return entidad;
            #endregion    
        }
        public static JavaList<Clientes> ListarClientes()
        {
            JavaList<Clientes> lista = new JavaList<Clientes>();
            try
            {
                lista.Add(new Clientes() { CodCliente = "164", NomCliente = "AGR�COLA HUARMEY S.A." });
                lista.Add(new Clientes() { CodCliente = "178", NomCliente = "AGROINDUSTRIAS SAN JACINTO S.A.A." });
                lista.Add(new Clientes() { CodCliente = "153", NomCliente = "ALGODONERA CONTINENTAL S.A.C." });
                lista.Add(new Clientes() { CodCliente = "172", NomCliente = "AMERICATEL PER� S.A." });
                lista.Add(new Clientes() { CodCliente = "139", NomCliente = "ANABI S.A.C." });
                lista.Add(new Clientes() { CodCliente = "26", NomCliente = "APUMAYO S.A.C." });
                lista.Add(new Clientes() { CodCliente = "157", NomCliente = "AUSTRAL GROUP S.A." });
                lista.Add(new Clientes() { CodCliente = "135", NomCliente = "Banco de Cr�dito del Per�" });
                lista.Add(new Clientes() { CodCliente = "176", NomCliente = "CARTAVIO S.A.A" });
                lista.Add(new Clientes() { CodCliente = "156", NomCliente = "CASA GRANDE S.A." });
                lista.Add(new Clientes() { CodCliente = "136", NomCliente = "CERAMICA LIMA S.A." });
                lista.Add(new Clientes() { CodCliente = "10000001", NomCliente = "CLIENTE_ENGIE ENERG�A PER�" });
                lista.Add(new Clientes() { CodCliente = "57", NomCliente = "COMPANIA MINERA ANTAMINA S.A." });
                lista.Add(new Clientes() { CodCliente = "5", NomCliente = "COMPA�IA MINERA ANTAPACCAY S.A." });
                lista.Add(new Clientes() { CodCliente = "158", NomCliente = "COMPA��A MINERA CHUNGAR S.A.C." });
                lista.Add(new Clientes() { CodCliente = "20", NomCliente = "COMPA�IA MINERA SANTA LUISA S.A." });
                lista.Add(new Clientes() { CodCliente = "104", NomCliente = "CONSORCIO ELECTRICO DE VILLACURI S.A.C." });
                lista.Add(new Clientes() { CodCliente = "174", NomCliente = "CORPORACI�N DE INDUSTRIAS PL�STICAS S.A." });
                lista.Add(new Clientes() { CodCliente = "32", NomCliente = "EDECA�ETE S.A." });
                lista.Add(new Clientes() { CodCliente = "86", NomCliente = "EDELNOR S.A.A." });
                lista.Add(new Clientes() { CodCliente = "33", NomCliente = "ELECTRO DUNAS S.A.A." });
                lista.Add(new Clientes() { CodCliente = "34", NomCliente = "ELECTRO PUNO S.A.A." });
                lista.Add(new Clientes() { CodCliente = "112", NomCliente = "ELECTRO SUR ESTE S.A.A." });
                lista.Add(new Clientes() { CodCliente = "114", NomCliente = "ELECTRO UCAYALI S.A." });
                lista.Add(new Clientes() { CodCliente = "88", NomCliente = "ELECTROCENTRO S.A." });
                lista.Add(new Clientes() { CodCliente = "89", NomCliente = "ELECTRONOROESTE S.A." });
                lista.Add(new Clientes() { CodCliente = "113", NomCliente = "ELECTRONORTE S.A." });
                lista.Add(new Clientes() { CodCliente = "90", NomCliente = "ELECTROSUR S.A." });
                lista.Add(new Clientes() { CodCliente = "138", NomCliente = "ESMERALDA CORP. S.A.C" });
                lista.Add(new Clientes() { CodCliente = "179", NomCliente = "FARMEX" });
                lista.Add(new Clientes() { CodCliente = "175", NomCliente = "GLORIA S.A" });
                lista.Add(new Clientes() { CodCliente = "92", NomCliente = "HIDRANDINA S.A." });
                lista.Add(new Clientes() { CodCliente = "23", NomCliente = "INDUSTRIAL PAPELERA ATLAS S.A." });
                lista.Add(new Clientes() { CodCliente = "159", NomCliente = "INDUSTRIAS FIBRAFORTE S.A." });
                lista.Add(new Clientes() { CodCliente = "165", NomCliente = "INMOBILIARIA BOTAFOGO S.A.C." });
                lista.Add(new Clientes() { CodCliente = "162", NomCliente = "INMOBILIARIA EL QUINDE S.A.C." });
                lista.Add(new Clientes() { CodCliente = "166", NomCliente = "INMOBILIARIA KANDOO S.A.C." });
                lista.Add(new Clientes() { CodCliente = "169", NomCliente = "INMOBILIARIA PISAC S.A.C." });
                lista.Add(new Clientes() { CodCliente = "167", NomCliente = "INVERSIONES ALAMEDA SUR S.A.C." });
                lista.Add(new Clientes() { CodCliente = "160", NomCliente = "INVERSIONES NACIONALES DE TURISMO S.A." });
                lista.Add(new Clientes() { CodCliente = "168", NomCliente = "INVERSIONES VILLA EL SALVADOR S.A.C." });
                lista.Add(new Clientes() { CodCliente = "29", NomCliente = "LINDE GAS PERU S.A." });
                lista.Add(new Clientes() { CodCliente = "31", NomCliente = "LUZ DEL SUR S.A.A." });
                lista.Add(new Clientes() { CodCliente = "3", NomCliente = "MANUFACTURA DE METALES Y ALUMINIO RECORD S.A." });
                lista.Add(new Clientes() { CodCliente = "11", NomCliente = "MINERA BATEAS S.A.C." });
                lista.Add(new Clientes() { CodCliente = "30", NomCliente = "MINERA LAS BAMBAS S.A." });
                lista.Add(new Clientes() { CodCliente = "151", NomCliente = "MINERA YANACOCHA S.R.L." });
                lista.Add(new Clientes() { CodCliente = "137", NomCliente = "MINSUR S.A." });
                lista.Add(new Clientes() { CodCliente = "12", NomCliente = "NYRSTAR CORICANCHA S.A." });
                lista.Add(new Clientes() { CodCliente = "152", NomCliente = "ORIGAN S.A." });
                lista.Add(new Clientes() { CodCliente = "22", NomCliente = "OWENS ILLINOIS PERU S.A." });
                lista.Add(new Clientes() { CodCliente = "27", NomCliente = "PAPELERA DEL SUR S.A." });
                lista.Add(new Clientes() { CodCliente = "8", NomCliente = "PAPELERA NACIONAL S.A." });
                lista.Add(new Clientes() { CodCliente = "163", NomCliente = "PARQUE LAMBRAMANI S.A.C." });
                lista.Add(new Clientes() { CodCliente = "21", NomCliente = "PETROLEOS DEL PERU PETROPERU S.A." });
                lista.Add(new Clientes() { CodCliente = "171", NomCliente = "PISOPAK Per� S.A.C." });
                lista.Add(new Clientes() { CodCliente = "7", NomCliente = "QUIMPAC S.A." });
                lista.Add(new Clientes() { CodCliente = "93", NomCliente = "SOCIEDAD ELECTRICA DEL SUR OESTE S.A." });
                lista.Add(new Clientes() { CodCliente = "134", NomCliente = "SOCIEDAD MINERA CERRO VERDE" });
                lista.Add(new Clientes() { CodCliente = "1", NomCliente = "SOUTHERN PERU COPPER CORPORATION" });
                lista.Add(new Clientes() { CodCliente = "155", NomCliente = "TRUPAL S.A." });
                lista.Add(new Clientes() { CodCliente = "2", NomCliente = "UNIVERSIDAD DE LIMA" });
                lista.Add(new Clientes() { CodCliente = "144", NomCliente = "VOTORANTIM METAIS - CAJAMARQUILLA S.A." });
                lista.Add(new Clientes() { CodCliente = "150", NomCliente = "YURA S.A." });

                return (lista);
            }
            catch
            {
                return (lista);
            }

        }
        public static JavaList<PuntoFacturacion> ListarPuntoFacturacion(string CodCliente)
        {
            JavaList<PuntoFacturacion> lista = new JavaList<PuntoFacturacion>();
            try
            {
                if (CodCliente == "1")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1219", DescPuntoFacturacion = "SPCC" });
                }
                if (CodCliente == "104")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1054", DescPuntoFacturacion = "Independencia 60 kV" });
                }
                if (CodCliente == "11")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "601", DescPuntoFacturacion = "BATEAS" });
                }
                if (CodCliente == "102")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "132", DescPuntoFacturacion = "QUENCORO 33" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "133", DescPuntoFacturacion = "SAN GABAN 138" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1028", DescPuntoFacturacion = "CUZCO 10,6" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1029", DescPuntoFacturacion = "MACHUPICCHU 10,6" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1030", DescPuntoFacturacion = "MACHUPICCHU 60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1031", DescPuntoFacturacion = "ABANCAY 138" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1032", DescPuntoFacturacion = "CACHIMAYO 60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1033", DescPuntoFacturacion = "CACHIMAYO 10,6" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1034", DescPuntoFacturacion = "CACHIMAYO 33" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1035", DescPuntoFacturacion = "CACHIMAYO 22,9" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1036", DescPuntoFacturacion = "COMBAPATA 60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1037", DescPuntoFacturacion = "COMBAPATA 22,9" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1038", DescPuntoFacturacion = "TINTAYA 10,6" });
                }

                if (CodCliente == "113")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "5", DescPuntoFacturacion = "CARHUAQUERO" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "117", DescPuntoFacturacion = "CHICLAYO OESTE60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "185", DescPuntoFacturacion = "CERRO CORONA 220" });
                }

                if (CodCliente == "114")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1047", DescPuntoFacturacion = "PUCALLPA_60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1048", DescPuntoFacturacion = "AGUAYTIA22_9" });
                }
                if (CodCliente == "12")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1236", DescPuntoFacturacion = "NYRSTAR" });
                }
                if (CodCliente == "134")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1227", DescPuntoFacturacion = "CERRO_VERDE" });
                }
                if (CodCliente == "135")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1230", DescPuntoFacturacion = "BCP_PRINCIPAL" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1231", DescPuntoFacturacion = "BCP_MELGAREJO" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1468", DescPuntoFacturacion = "BCP_CHORRILLOS" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1470", DescPuntoFacturacion = "BCP_SANISIDRO" });
                }
                if (CodCliente == "136")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1233", DescPuntoFacturacion = "CERAMICA_LIMA" });
                }
                if (CodCliente == "137")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1240", DescPuntoFacturacion = "MINSUR_FUNDICION" });
                }
                if (CodCliente == "138")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1242", DescPuntoFacturacion = "ESMERALDA_1454791" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1280", DescPuntoFacturacion = "ESMERALDA_2" });
                }
                if (CodCliente == "139")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1244", DescPuntoFacturacion = "ANABI" });
                }
                if (CodCliente == "144")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1311", DescPuntoFacturacion = "CAJAMARQUILLA VENTASPOT" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1403", DescPuntoFacturacion = "CAJAMARQUILLA_F" });
                }
                if (CodCliente == "150")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1313", DescPuntoFacturacion = "YURA" });
                }
                if (CodCliente == "151")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1322", DescPuntoFacturacion = "YANACOCHA" });
                }
                if (CodCliente == "152")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1324", DescPuntoFacturacion = "ORIGAN" });
                }
                if (CodCliente == "153")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1329", DescPuntoFacturacion = "ALGODONERA" });
                }
                if (CodCliente == "155")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1350", DescPuntoFacturacion = "TRUPAL_LIMA" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1443", DescPuntoFacturacion = "TRUPAL_TRUJ" });
                }
                if (CodCliente == "156")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1352", DescPuntoFacturacion = "GCAS_TRUJ_F" });
                }
                if (CodCliente == "157")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1360", DescPuntoFacturacion = "AUSTRAL" });
                }
                if (CodCliente == "158")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1371", DescPuntoFacturacion = "V-CHUNGAR" });
                }
                if (CodCliente == "159")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1373", DescPuntoFacturacion = "FIBRAFORTE" });
                }
                if (CodCliente == "160")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1378", DescPuntoFacturacion = "WESTIN" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1379", DescPuntoFacturacion = "PALACIO_INKA" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1380", DescPuntoFacturacion = "TAMBO_INKA" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1381", DescPuntoFacturacion = "LIBERTADOR_PARACAS" });
                }
                if (CodCliente == "162")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1390", DescPuntoFacturacion = "EL QUINDE" });
                }
                if (CodCliente == "163")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1386", DescPuntoFacturacion = "LAMBRAMANI" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1388", DescPuntoFacturacion = "LARCOMAR" });
                }
                if (CodCliente == "164")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1405", DescPuntoFacturacion = "AGRICOLA HUARMEY" });
                }
                if (CodCliente == "165")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1396", DescPuntoFacturacion = "BOTAFOGO" });
                }
                if (CodCliente == "166")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1398", DescPuntoFacturacion = "KANDOO" });
                }
                if (CodCliente == "167")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1392", DescPuntoFacturacion = "ALAMEDA SUR" });
                }
                if (CodCliente == "168")
                {lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1394", DescPuntoFacturacion = "VILLA EL SALVADOR" });
                }
                if (CodCliente == "169")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1407", DescPuntoFacturacion = "PISAC" });
                }
                if (CodCliente == "171")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1410", DescPuntoFacturacion = "PISOPAK - ATE" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1413", DescPuntoFacturacion = "PISOPAK - CAJAMARQUILLA" });
                }
                if (CodCliente == "172")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1440", DescPuntoFacturacion = "AMERICATEL " });
                }
                if (CodCliente == "174")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1447", DescPuntoFacturacion = "CIPSA_PLANT1" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1451", DescPuntoFacturacion = "CIPSA_PLANT2" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1452", DescPuntoFacturacion = "CIPSA_PLANT3" });
                }
                if (CodCliente == "175")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1445", DescPuntoFacturacion = "GLORIA_LIMA" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1460", DescPuntoFacturacion = "GLORIA_ARQP" });
                }
                if (CodCliente == "176")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1462", DescPuntoFacturacion = "CARTAVIO" });
                }

                if (CodCliente == "178")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1464", DescPuntoFacturacion = "SAN_JACINTO" });
                }

                if (CodCliente == "179")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1466", DescPuntoFacturacion = "FARM_PM0754_FACT" });
                }

                if (CodCliente == "2")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "603", DescPuntoFacturacion = "UNIVERSIDAD_LIMA" });
                }

                if (CodCliente == "20")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "576", DescPuntoFacturacion = "SANTA_LUISA" });
                }

                if (CodCliente == "21")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "591", DescPuntoFacturacion = "PUNTO DE SUMINISTRO TALARA 33 kV" });
                }

                if (CodCliente == "22")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "87", DescPuntoFacturacion = "OWENS_CALLAO" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1235", DescPuntoFacturacion = "OWENS_LURIN" });
                }

                if (CodCliente == "23")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "597", DescPuntoFacturacion = "ATLAS" });
                }

                if (CodCliente == "26")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "587", DescPuntoFacturacion = "APUMAYO" });
                }

                if (CodCliente == "27")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "585", DescPuntoFacturacion = "PAPELERA_SUR" });
                }

                if (CodCliente == "29")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "609", DescPuntoFacturacion = "LINDE" });
                }

                if (CodCliente == "3")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "595", DescPuntoFacturacion = "RECORD" });
                }

                if (CodCliente == "30")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1020", DescPuntoFacturacion = "LAS_BAMBAS" });
                }

                if (CodCliente == "31")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "139", DescPuntoFacturacion = "�A�A 61" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "140", DescPuntoFacturacion = "HUACHIPA 60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "144", DescPuntoFacturacion = "CALLAHUANCA 10" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1014", DescPuntoFacturacion = "SANTA ROSA 212" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1021", DescPuntoFacturacion = "SAN JUAN 212" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1022", DescPuntoFacturacion = "CHILCA 218" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1023", DescPuntoFacturacion = "SALAMANCA 61" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1024", DescPuntoFacturacion = "CHOSICA 63" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1041", DescPuntoFacturacion = "INDUSTRIAL 220" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1059", DescPuntoFacturacion = "ASIA 218 " });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1060", DescPuntoFacturacion = "ALTO PRADERAS 218" });
                }

                if (CodCliente == "32")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "75", DescPuntoFacturacion = "CANTERA 220" });
                }

                if (CodCliente == "34")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "123", DescPuntoFacturacion = "PUNO 60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "124", DescPuntoFacturacion = "PUNO 22,9" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "125", DescPuntoFacturacion = "JULIACA 10" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "126", DescPuntoFacturacion = "AYAVIRI 22,9" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "127", DescPuntoFacturacion = "AYAVIRI 10" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "128", DescPuntoFacturacion = "AZANGARO 22,9" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1026", DescPuntoFacturacion = "AZANGARO II 60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1057", DescPuntoFacturacion = "SAN GABAN 13.8" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1058", DescPuntoFacturacion = "JULIACA 22.9" });
                }
                if (CodCliente == "5")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "863", DescPuntoFacturacion = "ANTAPACCAY_OXIDOS" });
                }
                if (CodCliente == "57")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "580", DescPuntoFacturacion = "ANTAMINA_VIZCARRA" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "581", DescPuntoFacturacion = "ANTAMINA_HUARMEY" });
                }
                if (CodCliente == "7")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "589", DescPuntoFacturacion = "QUIMPAC_PARAMONGA" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "648", DescPuntoFacturacion = "QUIMPAC_OQUENDO" });
                }
                if (CodCliente == "8")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "583", DescPuntoFacturacion = "PANASA" });
                }
                if (CodCliente == "86")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "109", DescPuntoFacturacion = "PARAMONGA NUEVA 66" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "138", DescPuntoFacturacion = "SANTA ROSA 60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "196", DescPuntoFacturacion = "CARABAYLLO 220" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1013", DescPuntoFacturacion = "CHAVARRIA 212" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1014", DescPuntoFacturacion = "SANTA ROSA 212" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1015", DescPuntoFacturacion = "VENTANILLA 213" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1016", DescPuntoFacturacion = "HUACHO 66" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1017", DescPuntoFacturacion = "ZAPALLAL 220" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1055", DescPuntoFacturacion = "LOMERA 60" });
                }
                if (CodCliente == "88")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "19", DescPuntoFacturacion = "PACHACHACA" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "20", DescPuntoFacturacion = "OROYA50" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "86", DescPuntoFacturacion = "POMACOCHA220" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "87", DescPuntoFacturacion = "PARAGSHA II 138" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "100", DescPuntoFacturacion = "COBRIZA II 69" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "101", DescPuntoFacturacion = "COBRIZA II 10" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "103", DescPuntoFacturacion = "HUAYUCACHI 60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "104", DescPuntoFacturacion = "HUANCAVELICA 60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "105", DescPuntoFacturacion = "HUANCAVELICA 10" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "106", DescPuntoFacturacion = "AUCAYACU22,9" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "167", DescPuntoFacturacion = "CONDORCOCHA 44" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "168", DescPuntoFacturacion = "HUANUCO 22.9" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "169", DescPuntoFacturacion = "HUANUCO 10" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "170", DescPuntoFacturacion = "HUAYUCACHI 10" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "171", DescPuntoFacturacion = "TINGO MARIA 10" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "204", DescPuntoFacturacion = "CARHUAM 138" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1009", DescPuntoFacturacion = "YAUPI 13,8" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1010", DescPuntoFacturacion = "COBRIZA I 66" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1012", DescPuntoFacturacion = "HUALLANCA NUEVA 60" });
                }

                if (CodCliente == "89")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "66", DescPuntoFacturacion = "MALACAS" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "77", DescPuntoFacturacion = "PIURA60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "153", DescPuntoFacturacion = "PIURA 10" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1000", DescPuntoFacturacion = "MANCORA 10" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1001", DescPuntoFacturacion = "MANCORA 22.9" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1003", DescPuntoFacturacion = "NUEVO ZORRITOS 60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1004", DescPuntoFacturacion = "ZORRITOS 33" });
                }

                if (CodCliente == "90")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "146", DescPuntoFacturacion = "MONTALVO 138" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "147", DescPuntoFacturacion = "ILO 138" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "148", DescPuntoFacturacion = "TOMASIRI 66" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "149", DescPuntoFacturacion = "SARITA 33" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "150", DescPuntoFacturacion = "ARICOTA1 10,5" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "151", DescPuntoFacturacion = "ARICOTA2 10,5" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "163", DescPuntoFacturacion = "TACNA_66" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1039", DescPuntoFacturacion = "SOCABAYA 33" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1040", DescPuntoFacturacion = "HUAYTIRE 138" });
                }

                if (CodCliente == "92")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "109", DescPuntoFacturacion = "PARAMONGA NUEVA 66" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "110", DescPuntoFacturacion = "CHIMBOTE1 13,8" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "111", DescPuntoFacturacion = "HUALLANCA 66" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "112", DescPuntoFacturacion = "HUALLANCA 13,8" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "113", DescPuntoFacturacion = "CHIMBOTE1 138" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "114", DescPuntoFacturacion = "CHIMBOTE2 138" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "115", DescPuntoFacturacion = "GUADALUPE60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "116", DescPuntoFacturacion = "GUADALUPE10" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "189", DescPuntoFacturacion = "PORCON10" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "190", DescPuntoFacturacion = "LAPAJ22.9" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1005", DescPuntoFacturacion = "TRUJILLO NORTE 138" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1006", DescPuntoFacturacion = "TRUJILLO NORTE 10" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1007", DescPuntoFacturacion = "HUALLANCA 138" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1019", DescPuntoFacturacion = "KIMAN AYLLU 10" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1020", DescPuntoFacturacion = "HUARAZ OESTE 138" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1046", DescPuntoFacturacion = "CAJAMARCA 60" });
                }

                if (CodCliente == "93")
                {
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "57", DescPuntoFacturacion = "CALLALLI 138" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "64", DescPuntoFacturacion = "MOLLENDO 138" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "65", DescPuntoFacturacion = "REPARTICION 138" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "97", DescPuntoFacturacion = "CHILINA33" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "122", DescPuntoFacturacion = "MARCONA 60" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "137", DescPuntoFacturacion = "SANTUARIO 13,8" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1039", DescPuntoFacturacion = "SOCABAYA 33" });
                    lista.Add(new PuntoFacturacion() { IdPuntoFacturacion = "1066", DescPuntoFacturacion = "SOCABAYA 138" });
                }    
                return lista;
            }
            catch
            {
                return lista;
            }
        }

        public static JavaList<Medidor> ListarMedidor(string CodCliente)
        {
            JavaList<Medidor> lista = new JavaList<Medidor>();
            try
            {
                if (CodCliente == "10000001")
                {
                    lista.Add(new Medidor() { CodMedidor = "3", DescMedidor = "CT_CHILCA_PLUS.TG" });
                    lista.Add(new Medidor() { CodMedidor = "93", DescMedidor = "CH_QUITARACSA.TRAFO2" });
                    lista.Add(new Medidor() { CodMedidor = "137", DescMedidor = "QUIMPAC.PAR_ALCALIS" });
                }
                if (CodCliente == "11")
                {
                    lista.Add(new Medidor() { CodMedidor = "166", DescMedidor = "BATEAS.CAYLLOMA15" });
                }
                if (CodCliente == "112")
                {
                    lista.Add(new Medidor() { CodMedidor = "157", DescMedidor = "CIA_LAS_BAMBAS.L_2055_TERNA1" });
                    lista.Add(new Medidor() { CodMedidor = "161", DescMedidor = "CIA_LAS_BAMBAS.L_2055_TERNA1_1" });
                    lista.Add(new Medidor() { CodMedidor = "167", DescMedidor = "INTURSA.CUSCO" });
                    lista.Add(new Medidor() { CodMedidor = "170", DescMedidor = "INTURSA.URUBAMBA" });
                }
                if (CodCliente == "134")
                {
                    lista.Add(new Medidor() { CodMedidor = "162", DescMedidor = "CIA_CERRO_VERDE.L_1028_138_kV" });
                }
                if (CodCliente == "136")
                {
                    lista.Add(new Medidor() { CodMedidor = "156", DescMedidor = "CELIMA.CELIMA_LURIN" });
                }
                if (CodCliente == "137")
                {
                    lista.Add(new Medidor() { CodMedidor = "164", DescMedidor = "MINSUR.FUNDICION" });
                }
                if (CodCliente == "138")
                {
                    lista.Add(new Medidor() { CodMedidor = "146", DescMedidor = "ESMERALDACORP.ESMERALDA" });
                }
                if (CodCliente == "151")
                {
                    lista.Add(new Medidor() { CodMedidor = "153", DescMedidor = "CIA_YANACOCHA.L6648" });
                    lista.Add(new Medidor() { CodMedidor = "155", DescMedidor = "CIA_YANACOCHA.L2261" });
                    lista.Add(new Medidor() { CodMedidor = "160", DescMedidor = "CIA_YANACOCHA.L6649" });
                }
                if (CodCliente == "160")
                {
                    lista.Add(new Medidor() { CodMedidor = "167", DescMedidor = "INTURSA.CUSCO" });
                    lista.Add(new Medidor() { CodMedidor = "170", DescMedidor = "INTURSA.URUBAMBA" });
                    lista.Add(new Medidor() { CodMedidor = "191", DescMedidor = "INTURSA.PARACAS" });
                }
                if (CodCliente == "162")
                {
                    lista.Add(new Medidor() { CodMedidor = "192", DescMedidor = "PARQUE_ARAUCO.QUINDE" });
                }
                if (CodCliente == "163")
                {
                    lista.Add(new Medidor() { CodMedidor = "168", DescMedidor = "LAMBRAMANI.MALL_No_1" });
                    lista.Add(new Medidor() { CodMedidor = "169", DescMedidor = "LAMBRAMANI.MALL_No_2" });
                }
                if (CodCliente == "165")
                {
                    lista.Add(new Medidor() { CodMedidor = "171", DescMedidor = "MEGA_PLAZA.BOTAFOGO" });
                }
                if (CodCliente == "166")
                {
                    lista.Add(new Medidor() { CodMedidor = "189", DescMedidor = "MEGA_PLAZA.KANDOO" });
                }
                if (CodCliente == "175")
                {
                    lista.Add(new Medidor() { CodMedidor = "193", DescMedidor = "GLORIA.AREQUIPA" });
                }
                if (CodCliente == "20")
                {
                    lista.Add(new Medidor() { CodMedidor = "148", DescMedidor = "SANTALUISA.L2262_R" });
                }
                if (CodCliente == "22")
                {
                    lista.Add(new Medidor() { CodMedidor = "141", DescMedidor = "OWENS_ILLINOIS.OWENS" });
                    lista.Add(new Medidor() { CodMedidor = "159", DescMedidor = "OWENS_ILLINOIS.OWENS_LURIN" });
                }
                if (CodCliente == "23")
                {
                    lista.Add(new Medidor() { CodMedidor = "163", DescMedidor = "PAPELERA_ATLAS.PAPELERA_ATLAS" });
                }
                if (CodCliente == "26")
                {
                    lista.Add(new Medidor() { CodMedidor = "165", DescMedidor = "ARUNTANI.MINERA_APUMAYO" });
                }
                if (CodCliente == "27")
                {
                    lista.Add(new Medidor() { CodMedidor = "187", DescMedidor = "PAPELERA_DEL_SUR.PAPELERA_DEL_SUR" });
                }
                if (CodCliente == "3")
                {
                    lista.Add(new Medidor() { CodMedidor = "150", DescMedidor = "RECORD.Record_0485399" });
                }
                if (CodCliente == "30")
                {
                    lista.Add(new Medidor() { CodMedidor = "157", DescMedidor = "CIA_LAS_BAMBAS.L_2055_TERNA1" });
                    lista.Add(new Medidor() { CodMedidor = "161", DescMedidor = "CIA_LAS_BAMBAS.L_2055_TERNA1_1" });
                }
                if (CodCliente == "33")
                {
                    lista.Add(new Medidor() { CodMedidor = "171", DescMedidor = "MEGA_PLAZA.BOTAFOGO" });
                    lista.Add(new Medidor() { CodMedidor = "187", DescMedidor = "PAPELERA_DEL_SUR.PAPELERA_DEL_SUR" });
                    lista.Add(new Medidor() { CodMedidor = "189", DescMedidor = "MEGA_PLAZA.KANDOO" });
                    lista.Add(new Medidor() { CodMedidor = "191", DescMedidor = "INTURSA.PARACAS" });
                    lista.Add(new Medidor() { CodMedidor = "192", DescMedidor = "PARQUE_ARAUCO.QUINDE" });
                }
                if (CodCliente == "5")
                {
                    lista.Add(new Medidor() { CodMedidor = "140", DescMedidor = "XSTRATA.OXIDOS" });
                }
                if (CodCliente == "57")
                {
                    lista.Add(new Medidor() { CodMedidor = "142", DescMedidor = "CIA_ANTAMINA.PL_HUARMEY" });
                    lista.Add(new Medidor() { CodMedidor = "190", DescMedidor = "CIA_ANTAMINA.L_2255" });
                }
                if (CodCliente == "7")
                {
                    lista.Add(new Medidor() { CodMedidor = "137", DescMedidor = "QUIMPAC.PAR_ALCALIS" });
                    lista.Add(new Medidor() { CodMedidor = "145", DescMedidor = "QUIMPAC.OQU_261901" });
                }
                if (CodCliente == "8")
                {
                    lista.Add(new Medidor() { CodMedidor = "138", DescMedidor = "PANASA.U25" });
                    lista.Add(new Medidor() { CodMedidor = "139", DescMedidor = "SEPAEX.C136" });
                    lista.Add(new Medidor() { CodMedidor = "147", DescMedidor = "PANASA.U21" });
                    lista.Add(new Medidor() { CodMedidor = "149", DescMedidor = "PANASA.U11" });
                    lista.Add(new Medidor() { CodMedidor = "152", DescMedidor = "PANASA.U9" });
                    lista.Add(new Medidor() { CodMedidor = "154", DescMedidor = "PANASA.U53" });
                    lista.Add(new Medidor() { CodMedidor = "158", DescMedidor = "PANASA.U26" });
                }
                if (CodCliente == "93")
                {
                    lista.Add(new Medidor() { CodMedidor = "168", DescMedidor = "LAMBRAMANI.MALL_No_1" });
                    lista.Add(new Medidor() { CodMedidor = "169", DescMedidor = "LAMBRAMANI.MALL_No_2" });
                    lista.Add(new Medidor() { CodMedidor = "193", DescMedidor = "GLORIA.AREQUIPA" });
                }
                return lista;
            }
            catch
            {
                return lista;
            }
        }
    }
}