﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace E7.Droid
{
    [Activity(Label = "NpassActivity", Theme = "@android:style/Theme.NoTitleBar")]
    public class NpassActivity : Activity
    {
        Button btn1;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Layout.npass);

            btn1 = FindViewById<Button>(Resource.Id.button1);


            btn1.Click += (sender, e) => inicio();



            void inicio()
            {
                StartActivity(typeof(InicioActivity));
            }
        }
    }
}