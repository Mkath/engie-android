﻿using System;
using System.Collections.Generic;
using System.Net.Http;

namespace E7.Droid
{
    public class COConsumoFacturado
    {
        public List<BEConsumoFacturado> Reporte_ConsumoFacturado(string Anio, string CodCliente, string CodPuntoFacturacion)
        {
            #region Método Get Listar Consumo Facturado
            string Api = "http://appwebservice.engie-energia.pe/ServicioPrueba/Api/SReportes/";

            List<BEConsumoFacturado> entidad = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Api);

                // Realizar la petición GET
                HttpResponseMessage response = client.GetAsync("ReporteC_ConsumoFacturado?Anio=" + Anio + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion).Result;

                if (response.IsSuccessStatusCode)
                {
                    var res = response.Content.ReadAsStringAsync().Result;
                    entidad = (List<BEConsumoFacturado>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEConsumoFacturado>>(res);
                }
            }
            return entidad;
            #endregion  
        }
    }
}