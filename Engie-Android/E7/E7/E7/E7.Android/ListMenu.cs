﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace E7.Droid
{
    public class ContentMenu
    {
        public int Icono { get; set; }
        public string Titulo { get; set; }
    }

    public class ListMenu
    {
        public static JavaList<ContentMenu> ListarMenu()
        {
            JavaList<ContentMenu> lista = new JavaList<ContentMenu>();
            try
            {
                lista.Add(new ContentMenu() { Icono = Resource.Drawable.ichome, Titulo = "Inicio" });
                lista.Add(new ContentMenu() { Icono = Resource.Drawable.consbtn, Titulo = "Consumo" });
                lista.Add(new ContentMenu() { Icono = Resource.Drawable.facbtn, Titulo = "Facturación" });
                lista.Add(new ContentMenu() { Icono = Resource.Drawable.logout, Titulo = "Cerrar Sessión" });
                return lista;
            }
            catch
            {
                return lista;
            }
        }
    }
}